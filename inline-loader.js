(() => {
  if (window.histreLoadedInline) {
    return true;
  }
  window.histreLoadedInline = true;

  // For Firefox
  if (typeof browser === 'object') { chrome = browser; }

  return false;
})();
