(function () {
  const apiUrl = `${apiBase}/visit/`;
  const historyUrl = `${apiBase}/history.handle.json`;
  const settingsUrl = `${apiBase}/settings/`;

  const POST_PERIOD = 1000 * (isDevMode ? 5 : 30);
  const clock = [];
  const navfrom = {};

  let lastTabId = -1;
  let lastPost = Date.now();
  let historyThreads = 0;
  let clientuuid = null;

  //?!
  function fetchSettings() {
    helper.doGet(settingsUrl, {});
  }

  function firstTab(tabs, caller) {
    if (
      typeof tabs === 'undefined' ||
      typeof tabs.length === 'undefined' ||
      typeof tabs[0] === 'undefined'
    ) {
      helper.llog(`${caller} : tab is not available`);
      return null;
    }
    return tabs[0];
  }

  // declared in background-globals.js
  enableOrDisableHistory = function () {
    const enable = _log_account && _log_browser;
    if (enable) {
      enableHistory();
    } else {
      disableHistory();
    }
  };

  function cleanFavicon(favicon) {
    if (favicon && (favicon.indexOf('http') !== 0 || favicon.length > 255)) {
      return null;
    }
    return favicon;
  }

  // ---- Handlers ----

  function onInstalledHandler(details) {
    if (details.reason === 'install') {
      chrome.tabs.create({
        url: 'onboarding.html'
      });
    }

    if (details.reason === 'update') {
      runBookmarkImport();

      runStorageMigrations(details.previousVersion, refresh_from_storage);
    }
  }

  function onStartupHandler(object) {}

  function tabUpdatedHandler(tabId, changes, tab) {
    if (changes.url) {
      //url of the tab changing for some reason
      if (tab.active) {
        //start new click if its active tab and we are not already tracking it
        if (clock.length > 0) {
          var idx = clock.length - 1;
          if (clock[idx].tabId == tabId && clock[idx].url == changes.url) {
            return;
          }
        }
        startClock();
      }
    }
    if (changes.status != 'complete') {
      //do updates only if page already loaded
      return;
    }

    for (var l = clock.length - 1; l >= 0; l--) {
      if (clock[l].tabId == tab.id) {
        if (tab.favIconUrl) {
          clock[l].favicon = cleanFavicon(tab.favIconUrl);
        }
        if (tab.title) {
          clock[l].title = tab.title;
        }
        clock[l].navfrom = navfrom[tab.id];
        break;
      }
    }

    if (Date.now() - lastPost > POST_PERIOD) {
      // post only after a tab completes loading
      lastPost = Date.now();
      persist();
    }
  }

  function tabCreatedHandler(tab) {
    //left click that opens new tab, Ctrl/CMD-T
    if (tab.openerTabId) {
      //trying to get refefrrer for new tab.
      chrome.tabs.get(tab.openerTabId, function (oldtab) {
        if (oldtab) {
          navfrom[tab.id] = oldtab.url;
        }
      });
    }
  }

  function focusChangedHandler(window) {
    if (window == chrome.windows.WINDOW_ID_NONE) {
      helper.llog('lost focus');
      stopClock();
    } else {
      helper.llog('got focus');
      startClock();
    }
  }

  function onCommittedHandler(e) {
    //checking if navigation happened in addressbar
    for (var i = 0; i < e.transitionQualifiers.length; i++) {
      if (e.transitionQualifiers[i] == 'from_address_bar') {
        navfrom[e.tabId] = null; //user navigated from addressbar, clear navfrom for this tab. it's 100% active tab
        for (l = clock.length - 1; l >= 0; l--) {
          //also clear it in last clock record, it's already there. should be last one if we did not switched tab fast.
          if (clock[l].tabId == e.tabId && clock[l].url == e.url) {
            clock[l].navfrom = null;
            break;
          }
        }
      }
    }
  }

  function onBeforeNavigateHandler(e) {
    //new url, trying to get url of last open tab - it's current tab or tab that launched navigation in new tab
    if (e.frameId != 0 || lastTabId == -1) return;
    chrome.tabs.get(lastTabId, function (tab) {
      if (tab) {
        navfrom[e.tabId] = tab.url;
      }
    });
  }

  function startClock() {
    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
      stopClock();
      const tab = firstTab(tabs, 'startClock');
      if (tab === null || !tab.url.startsWith('http')) {
        return;
      }

      clock.push({
        url: tab.url,
        tabId: tab.id,
        start: Date.now(),
        end: null,
        title: tab.title,
        navfrom: navfrom[tab.id],
        favicon: cleanFavicon(tab.favIconUrl),
      });
      helper.llog(`startClock : ${tab.url}`);

      if (chrome.runtime.lastError) {
        helper.llog(`startClock Error : ${chrome.runtime.lastError}`)
      }

    });
  }

  function onActivatedHandler(activeInfo) {
    lastTabId = activeInfo.tabId;
    helper.llog('tab activated');
    startClock();
  }

  function stopClock() {
    if (clock.length > 0 && clock[clock.length - 1].end == null) {
      clock[clock.length - 1].end = Date.now();
      helper.llog('stopclock', JSON.stringify(clock));
    } else {
      helper.llog('cant stop clock coz none running');
    }
  }

  function persist() {
    const enable = _log_account && _log_browser;
    if (!enable) {
      helper.llog('in persist, but history not enabled');
      return;
    }
    let l = clock.length;
    if (l <= 0) {
      helper.llog('nothing to persist');
      return;
    }
    if (clock[l - 1].end == null) {
      l -= 1;
    }
    if (l <= 0) {
      helper.llog('nothing to persist');
      return;
    }
    let payload = [];
    let item;
    for (let i = 0; i < l; i++) {
      item = clock[i];
      if (
        item.url === 'about:blank' ||
        item.url.indexOf('https://www.google.com/_/chrome/newtab') > -1 ||
        item.url.indexOf('https://notifications.google.com') > -1
      ) {
        continue;
      }
      payload.push(item);
    }
    helper.llog(`persist: ${JSON.stringify(payload)}`);
    helper.doPost(apiUrl, payload, function (json) {
      if (json.error === false) {
        helper.llog('persisted ok');
        clock.splice(0, l);
        helper.unsetIconWarning();
      } else {
        const { status } = json;
        if (
          status === helper.HttpCodes.unauthorized ||
          status === helper.HttpCodes.forbidden
        ) {
          helper.llog('not authenticated.');
          helper.setIconWarning();
        }
      }
    });
  }

  function postHistory() {
    chrome.history.search({ text: '' }, function callback(history) {
      let data = [];
      for (var i = 0; i < history.length; ++i) {
        let url = history[i].url;
        let title = history[i].title;
        let lastVisitTime = history[i].lastVisitTime;
        var processVisits = function (url, title, lastVisitTime) {
          return function (visitItems) {
            for (var j = 0; j < visitItems.length; j++) {
              data.push({
                id: visitItems[j].id,
                url: url,
                title: title,
                datetime: lastVisitTime,
                referrerId: visitItems[j].referringVisitId,
              });
            }
            historyThreads--;
            if (historyThreads == 0) {
              helper.llog(data);
              helper.doPost(historyUrl, data, function (result) {
                helper.llog('History data sent' + result);
              });
            }
          };
        };
        historyThreads++;
        chrome.history.getVisits(
          { url: url },
          processVisits(url, title, lastVisitTime)
        );
      }
    });
  }

  function enableHistory() {
    helper.llog('enableHistory');
    chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
    chrome.tabs.onCreated.addListener(tabCreatedHandler);
    chrome.windows.onFocusChanged.addListener(focusChangedHandler);
    chrome.webNavigation.onCommitted.addListener(onCommittedHandler);
    chrome.webNavigation.onBeforeNavigate.addListener(onBeforeNavigateHandler);
    // Latest version of Chrome as of 2021-Jun-14 is resulting in the following error:
    // "Tabs cannot be edited right now (user may be dragging a tab)"
    // Ref: https://www.reddit.com/r/chrome_extensions/comments/no7igm/chrometabsonactivatedaddlistener_not_working/h0jf8vd/
    // So we use setTimeout to not get this error
    chrome.tabs.onActivated.addListener(() => {
      setTimeout(() => { onActivatedHandler }, 500);
    });
  }

  function disableHistory() {
    helper.llog('disableHistory');
    chrome.tabs.onUpdated.removeListener(tabUpdatedHandler);
    chrome.tabs.onCreated.removeListener(tabCreatedHandler);
    chrome.windows.onFocusChanged.removeListener(focusChangedHandler);
    chrome.webNavigation.onCommitted.removeListener(onCommittedHandler);
    chrome.webNavigation.onBeforeNavigate.removeListener(
      onBeforeNavigateHandler
    );
    chrome.tabs.onActivated.removeListener(onActivatedHandler);
  }

  function enableOther() {
    chrome.permissions.contains(
      {
        permissions: ['bookmarks'],
      },
      function (result) {
        if (result) {
          chrome.bookmarks.onCreated.addListener(bookmarksOnCreatedHandler);
          chrome.bookmarks.onRemoved.addListener(bookmarksOnDeletedHandler);
        } else {
          // we don't have the permission to do that
        }
      }
    );
  }

  chrome.runtime.onInstalled.addListener(onInstalledHandler);
  chrome.runtime.onStartup.addListener(onStartupHandler);
  chrome.runtime.onMessage.addListener(onMessageHandler);

  function onMessageHandler(request, sender, sendResponse) {
    try {
      if (sender.tab) {
        if (request.type == 'open_tabs') {
          chrome.windows.create({ url: request.urls }, (w) => {
            helper.llog(w);
            sendResponse(true);
          });
        }
      }
    } catch (e) {
      helper.llog(e);
    }
  }

  // listen to changes from popup
  chrome.storage.onChanged.addListener((changes, namespace) => {
    const { enablePermissions } = changes;
    refresh_from_storage();
    if (enablePermissions && enablePermissions.oldValue !== enablePermissions.newValue) {
      runBookmarkImport();
    }
  });

  function refresh_from_storage() {
    chrome.storage.local.get(
      [
        'userLogHistory',
        'featuresHistory',
        'enablePermissions',
        'postedHistory',
      ],
      (result) => {
        let log_account = result.userLogHistory;
        let log_browser = result.featuresHistory;
        if (typeof log_account === 'undefined') {
          log_account = true;
        }
        if (typeof log_browser === 'undefined') {
          log_browser = true;
        }
        _log_account = log_account;
        _log_browser = log_browser;
        enableOrDisableHistory();

        if (result.enablePermissions && !result.postedHistory) {
          chrome.permissions.contains(
            {
              permissions: ['history'],
            },
            function (result) {
              if (result) {
                postHistory();
                chrome.storage.local.set({ postedHistory: true }, () => {});
              } else {
                // we don't have the permission to do that
              }
            }
          );
        }
      }
    );
  }

  function initialize() {
    fetchSettings();
    enableOther();
    refresh_from_storage();
  }
  initialize();
})();
