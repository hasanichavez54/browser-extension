const histre_origins = [host + '/'];

const manifest = chrome.runtime.getManifest();
const version = manifest.version;

const delayUnitBetweenAPICalls = 200;  // ms

let _log_account;
let _log_browser;

let enableOrDisableHistory;
