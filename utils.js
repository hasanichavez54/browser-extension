(() => {
  if (typeof window.histreUtils === 'function') {
    return;
  }

  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const host = window.histreDOMHelper.getHost();

  class Utils {
    constructor(dest, refs) {
      this.dest = dest;
      this.refs = refs;
      if (dest === 'popup') {
        this.isPopup = true;
        this.prefix = (classNames) => classNames;
      } else {
        this.prefix = (classNames) => histreDOMHelper.prefix(dest, classNames);
      }
    }

    //----- Vote UI -----//

    createVoteUI(displaySpinner) {
      const prefix = this.prefix;

      const voteUpText = 'Vote up';
      const voteDownText = 'Vote down';
      const voteRemoveText = 'Remove vote';

      const $voteUp = $e('div', {
        class: prefix('vote-icon vote-icon--up'),
        title: voteUpText,
        'aria-label': voteUpText,
      }, { click: voteUp });
      const $voteDown = $e('div', {
        class: prefix('vote-icon vote-icon--down'),
        title: voteDownText,
        'aria-label': voteDownText,
      }, { click: voteDown });

      function updateVote(voteVal) {
        if (voteVal === 1) {
          $voteUp.classList.add(prefix(`vote-icon--up-sel`));
          $voteUp.setAttribute('title', voteRemoveText);
          $voteUp.setAttribute('aria-label', voteRemoveText);
          $voteDown.classList.remove(prefix(`vote-icon--down-sel`));
          $voteDown.setAttribute('title', voteDownText);
          $voteDown.setAttribute('aria-label', voteDownText);
        } else if (voteVal === -1) {
          $voteUp.classList.remove(prefix(`vote-icon--up-sel`));
          $voteUp.setAttribute('title', voteUpText);
          $voteUp.setAttribute('aria-label', voteUpText);
          $voteDown.classList.add(prefix(`vote-icon--down-sel`));
          $voteDown.setAttribute('title', voteRemoveText);
          $voteDown.setAttribute('aria-label', voteRemoveText);
        } else {
          $voteUp.classList.remove(prefix(`vote-icon--up-sel`));
          $voteUp.setAttribute('title', voteUpText);
          $voteUp.setAttribute('aria-label', voteUpText);
          $voteDown.classList.remove(prefix(`vote-icon--down-sel`));
          $voteDown.setAttribute('title', voteDownText);
          $voteDown.setAttribute('aria-label', voteDownText);
        }
      }

      function vote(action) {
        if (action !== 'up' && action !== 'down') {
          return;
        }

        const elem = action === 'up' ? $voteUp : $voteDown;

        if (elem.classList.contains(prefix(`vote-icon--${action}-sel`))) {
          // previously voted
          updateVote(0);
          postVote(0);
        } else {
          // previously unvoted
          const voteVal = (action === 'up') ? 1 : -1;
          updateVote(voteVal);
          postVote(voteVal);
        }
      };

      function postVote(voteVal) {
        const data = {
          action: 'saveInlineVote',
          dest: 'inline',
          inline: {
            url: location.href,
            title: document.title,
            vote: voteVal
          },
        };
        if (typeof displaySpinner === 'function') { displaySpinner(); };
        // persist vote on the backend
        try {
          chrome.runtime.sendMessage(data, function (response) {
            if (chrome.runtime.lastError) {
              console.log(JSON.stringify(chrome.runtime.lastError));
            }
            if (response && response.usermsg) {
              // TODO: Replace below with logic to show error message to user
              console.log(response.usermsg);
            }
          });
        } catch (e) {
          // connection to background scripts lost, perhaps due to extn reload
          // clear self etc so the user reloads the page
          nukeSelf();
        }
      };

      function voteUp() {
        vote('up');
      };

      function voteDown() {
        vote('down');
      };

      return {
        elements: [$voteUp, $voteDown],
        updateVote,
      };
    }

    //----- Access levels -----//

    createAccessLevels(accessLevels) {
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const { quill, saveNote } = this.refs;

      const $btnGroup = $e('div', { class: prefix('btn-group'), role: 'group', 'aria-label': 'Access level' });

      function createAccessSelector([choice, text]) {
        const $input = $e('input', {
          type: 'radio',
          id: prefix(`btn-access-${choice}`),
          class: prefix('btn-check'),
          name: 'access',
          value: choice,
          autocomplete: 'off',
        }, { change: () => { saveNote(); quill.focus(); } });

        let $icon;
        if (isPopup) {
          $icon = $e('i', { 'aria-label': text });
          if (choice === 'private') {
            $icon.className = 'fe fe-lock';
          } else if (choice === 'org') {
            $icon.className = 'fe fe-users';
          } else {
            $icon.className = 'fe fe-globe';
          }
        } else {
          $icon = $e('span', { class: prefix(`access-icon access-icon--${choice}`), 'aria-label': text });
        }

        const $label = $e('label', {
          class: prefix('btn btn-sm btn-outline-secondary'),
          for: prefix(`btn-access-${choice}`),
          title: text,
        }, noEvents, [$icon]);

        return [$input, $label];
      }

      accessLevels.forEach((accessLevel) => {
        const [$input, $label] = createAccessSelector(accessLevel);

        $btnGroup.appendChild($input);
        $btnGroup.appendChild($label);
      });

      return $btnGroup;
    }

    //----- Tags -----//

    createSuggestedTags(tags) {
      const prefix = this.prefix;
      const { quill } = this.refs;

      const $suggestedTagsText = $e('div', { class: prefix('suggested-tags-text') }, noEvents, [$t('Suggested Tags')]);
      const $suggestedTags = $e('div', { class: prefix('suggested-tags') }, noEvents, [$suggestedTagsText]);

      function addSuggestedTagToNote() {
        const tag = this.dataset.histreTagVal;
        const selection = quill.getSelection(true);
        const pos = selection.index;
        const txt = ` #${tag} `;
        quill.insertText(pos, txt, 'user');
        quill.setSelection(pos + txt.length, 0, 'user');
      };

      function adjustTag(tag) {
        const maxl = 12;
        const tail = 4;
        if (!tag) {
          return tag;
        }
        const l = tag.length;
        if (l <= maxl) {
          return tag;
        } else {
          return (
            tag.slice(0, Math.min(maxl - (tail + 3), l - tail)) +
            '...' +
            tag.slice(l - tail, l)
          );
        }
      };

      tags.forEach((tag) => {
        const tagHtml = $e(
          'div',
          { class: prefix('suggested-tag'), 'data-histre-tag-val': tag },
          { click: addSuggestedTagToNote },
          [$t(`#${adjustTag(tag)}`)],
        );
        $suggestedTags.appendChild(tagHtml);
      });

      return $suggestedTags;
    };

    //----- Data extraction -----//

    createExtractedDataTable() {
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const refs = this.refs;

      const $dataTable = $e('table', { id: prefix('extracted-data-table'), class: prefix('extracted-data-table') });
      const $newRow = $e('div', {
        class: prefix('extracted-data-new'),
        tabindex: '0',
        disabled: '',
      }, {
        click: addNewRow,
        keydown: addNewRowKeydown,
      }, [
        isPopup ? $e('i', { class: 'fe fe-plus' }) : $e('span', { class: prefix('extracted-data-icon extracted-data-icon--plus') }),
        $t('Add Attribute'),
      ]);

      function renderExtractedData(data) {
        if ($newRow.hasAttribute('disabled')) {
          $newRow.removeAttribute('disabled');
        }

        $dataTable.textContent = '';
        if (data) {
          for (let field in data) {
            let fieldValue = data[field];

            if (typeof fieldValue === 'boolean') {
              fieldValue = (fieldValue) ? 'Yes' : 'No';
            }

            $dataTable.appendChild(createRow(field, fieldValue));
          }
        }
      }

      function getTableData() {
        const fields = [...$dataTable.querySelectorAll('tr > th')].map((field) => field.textContent);
        if (fields.length > 0) {
          const values = [...$dataTable.querySelectorAll('tr > td')].map((vaule) => vaule.textContent);
          const attrs = {};
          for (let i = 0; i < fields.length; i++) {
            attrs[fields[i]] = values[i];
          }
          return attrs;
        } else {
          return null;
        }
      }

      function createRow(field, value) {
        const $rowHeader = $e('th', {
          class: prefix('extracted-data-row-header'),
          scope: 'row',
          contenteditable: 'true',
        }, { input: saveTable }, field ? [$t(toHeaderText(field))] : []);
        const $rowValue = $e('td', {
          class: prefix('extracted-data-row-value'),
          contenteditable: 'true',
        }, { input: saveTable }, value ? [$t(value)] : []);
        const $removeRow = $e('span', {
          class: prefix('extracted-data-remove'),
          tabindex: '0',
          title: 'Delete row',
          'aria-label': 'Delete row',
        }, {
          click: removeRow,
          keydown: removeRowKeydown,
        }, [
          isPopup ? $e('i', { class: 'fe fe-x' }) : $e('span', { class: prefix('extracted-data-icon extracted-data-icon--x') }),
        ]);
        const $row = $e('tr', { class: prefix('extracted-data-row') }, noEvents, [$rowHeader, $rowValue, $removeRow]);

        function removeRow() {
          const container = isPopup ? document : document.querySelector(`#${prefix('wrap')}`);

          function confirmDelete(e) {
            if (e.target.closest('tr > span') !== $removeRow) {
              $row.removeAttribute('data-histre-delete-row');
              container.removeEventListener('click', confirmDelete);
              $removeRow.setAttribute('title', 'Delete row');
              $removeRow.setAttribute('aria-label', 'Delete row');
            }
          }

          if ($row.hasAttribute('data-histre-delete-row')) {
            $row.remove();
            refs.saveNote();
            container.removeEventListener('click', confirmDelete);
          } else {
            $row.setAttribute('data-histre-delete-row', '');
            container.addEventListener('click', confirmDelete);
            this.setAttribute('title', 'Click again to delete row');
            this.setAttribute('aria-label', 'Click again to delete row');
          }
        }

        function removeRowKeydown(e) {
          if (e.key === 'Enter' || e.key === ' ') {
            e.preventDefault();
            removeRow();
          }
        }

        function saveTable() {
          if ($rowHeader.textContent === '') {
            function showFieldError() {
              $rowHeader.removeAttribute('data-histre-table-field-error');
              $rowHeader.removeAttribute('title');
              $rowHeader.removeAttribute('aria-label');
              $rowHeader.removeEventListener('input', showFieldError);
            }

            $rowHeader.setAttribute('data-histre-table-field-error', '');
            $rowHeader.setAttribute('title', 'Please enter this field');
            $rowHeader.setAttribute('aria-label', 'Please enter this field');
            $rowHeader.addEventListener('input', showFieldError);
          } else {
            refs.saveNoteDebounced();
          }
        }

        return $row;
      }

      function addNewRow() {
        if (this.hasAttribute('disabled')) {
          return;
        }
        const $row = createRow();
        const $field = $row.querySelector('th');
        $dataTable.appendChild($row);
        $field.focus();
      }

      function addNewRowKeydown(e) {
        if (e.key === 'Enter' || e.key === ' ') {
          e.preventDefault();
          addNewRow();
        }
      }

      function toHeaderText(fieldName) {
        // Convert snake-case field names to title case for header text
        if (!fieldName) {
          return '';
        }
        return fieldName.replace(/_+/g, ' ').replace(
          /\w\S*/g,
          (text) => {
            return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
          }
        );
      };

      return {
        elements: [$dataTable, $newRow],
        renderExtractedData,
        getTableData,
      };
    }

    //----- Recent books : Begin -----//

    createRecentBooks(initialRecentBooks) {
      const dest = this.dest;
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const { quill, saveNote, saveNoteDebounced } = this.refs;

      let recentBooks = initialRecentBooks;
      let shownRecentBooks = recentBooks;
      const maxBooksDisplayed = isPopup ? 3 : 4;

      //--- Header ---//
      const $recentBooksToggleIcon = isPopup ?
        $e('i', { class: 'fe fe-chevron-up' }) :
        $e('div', { class:  prefix('book-toggle-icon book-toggle-icon--hide') });
      const $recentBooksToggle = $e('button', { class: prefix('btn btn-sm btn-outline-secondary') }, noEvents, [$recentBooksToggleIcon]);
      const $recentBooksHeaderText = $e('div', { class: prefix('recent-books-toggle-text') }, noEvents, [
        $t('Add this note to collection(s) '), $e('span', {}, noEvents, [$t('(optional)')]),
      ]);
      // Search
      const $recentBooksSearchToggle = $e('button', {
        class: prefix('btn btn-sm btn-outline-secondary recent-books-search-toggle'),
        'data-histre-add-to-book-for': 'search',
      }, { click: toggleRecentBooksSearch }, [isPopup ?
        $e('i', { class: 'fe fe-search' }) :
        $e('div', { class: prefix('book-toggle-icon book-toggle-icon--search') }),
      ]);
      // Create
      const $recentBooksCreateToggle = $e('button', {
        class: prefix('btn btn-sm btn-outline-secondary'),
        'data-histre-add-to-book-for': 'create',
      }, { click: toggleRecentBooksCreate }, [isPopup ?
        $e('i', { class: prefix('fe fe-plus') }) :
        $e('div', { class: prefix('book-toggle-icon book-toggle-icon--create') }),
      ]);
      // Header element
      const $recentBooksHeader = $e('div', {
        class: prefix('recent-books-header'),
        role: 'toggle',
        'aria-label': 'Show Collections List',
      }, { click: toggleRecentBooks }, [
        $recentBooksCreateToggle, $recentBooksSearchToggle, $recentBooksHeaderText, $recentBooksToggle,
      ]);

      //--- Body ---//
      // Search
      const $recentBooksSearchInput = $e('input', {
        class: prefix('form-control form-control-sm recent-books-search-input'),
        type: 'text',
        placeholder: 'Search Collections',
        'data-histre-book-input': '',
      }, { input: handleRecentBooksSearch, keydown: handleSearchKeydown } );
      const $recentBooksSearchText = $e('div', { class: prefix('recent-books-search-text') }, noEvents, [$t('Type a collection title or keywords')]);
      const $recentBooksSearch = $e('div', { class: prefix('recent-books-search') }, noEvents , [$recentBooksSearchInput, $recentBooksSearchText]);
      // Create
      const $recentBooksCreateCheck = isPopup ?
        $e('i', { class: prefix('fe fe-circle') }) :
        $e('div', { class: prefix('book-icon book-icon--circle') });
      const $recentBooksCreateIcon = isPopup ?
        $e('i', { class: prefix('book-access fe fe-lock') }) :
        $e('div', { class: prefix('book-access book-icon book-icon--private') });
      const $recentBooksCreateInput = $e('input', {
        class: prefix('form-control form-control-sm'),
        type: 'text',
        placeholder: 'Title of the new collection',
        'data-histre-book-input': '',
      }, { keydown: handleBookCreateKeydown });
      const $recentBooksCreateAccept = $e(isPopup ? 'i' : 'div', {
        class: isPopup ? 'fe fe-check' : prefix('book-icon book-icon--create-check'),
        'aria-label': 'Create new collection',
        title: 'Create new collection',
      }, { click: saveNote });
      const $recentBooksCreateCancel = $e(isPopup ? 'i' : 'div' ,{
        class: isPopup ? 'fe fe-x' : prefix('book-icon book-icon--create-cancel'),
        'aria-label': 'Cancel collection creation',
        title: 'Cancel collection creation',
      }, { click: toggleRecentBooksCreate });
      const $recentBooksCreate = $e('div', { class: prefix('book-check-wrap book-check-wrap--create') }, noEvents, [
        $recentBooksCreateCheck, $recentBooksCreateIcon, $recentBooksCreateInput, $recentBooksCreateAccept, $recentBooksCreateCancel,
      ]);
      // Books
      const $recentBooksBooks = $e('div', { class: prefix('recent-books-books') });
      // Show more / count
      const $recentBooksCount = $e('span');
      const $recentBooksShowMoreChevrons = isPopup ?
        $e('i', { class: prefix('fe fe-chevrons-down recent-books-show-more-chevrons') }) :
        $e('div', { class: prefix('book-icon book-icon--chevrons') });
      const $recentBooksShowMore = $e('button', {
        class: isPopup ? 'btn btn-link p-0 mt-2' : prefix('btn btn-link btn-link--no-padding'),
      }, { click: showAllBooks }, [
        $recentBooksCount, $t(' selected. Show more'), $recentBooksShowMoreChevrons
      ]);
      const $recentBooksShowMoreWithoutCount = $e('button', {
        class: isPopup ? 'btn btn-link p-0 mt-2' : prefix('btn btn-link btn-link--no-padding'),
      }, { click: showAllBooks }, [
        $t('Show more'), $recentBooksShowMoreChevrons.cloneNode(true)
      ]);
      // Body element
      const $recentBooksBody = $e('div', { class: prefix('recent-books-body') }, noEvents, [
        $recentBooksSearch, $recentBooksCreate, $recentBooksBooks, $recentBooksShowMore, $recentBooksShowMoreWithoutCount
      ]);

      //--- Wrap ---//
      const $recentBooks = $e('div', { class: prefix('recent-books') }, noEvents, [$recentBooksHeader, $recentBooksBody]);

      function setupRecentBooks(isSearch = false) {
        $recentBooksBooks.textContent = '';
        $recentBooksShowMore.style.display = 'none';
        $recentBooksShowMoreWithoutCount.style.display = 'none';
        if (!shownRecentBooks) {
          return;
        }

        const orgAccessLabel = document.querySelector(`label[for=${prefix('btn-access-org')}]`)?.title;

        shownRecentBooks.forEach((book, bookIndex) => {
          const isAdded = book.current_url_item_id != null;

          const $bookItemWrap = $e('div', {
            id: prefix(`book-${book.book_id}`),
            class: prefix('book-wrap'),
          });
          if (bookIndex >= maxBooksDisplayed) {
            $bookItemWrap.style.display = 'none';
          }

          const $bookCheckWrap = $e('div', {
            class: prefix('book-check-wrap'),
            role: 'checkbox',
            'aria-checked': isAdded ? 'true' : 'false',
            tabindex: 0,
          }, { click: handleBookSelection });

          const $bookCheck = $e(isPopup ? 'i' : 'div', {
            class: isPopup ?
              `fe ${isAdded ? 'fe-check-circle' : 'fe-circle'}` :
              prefix(`book-icon ${isAdded ? 'book-icon--checked' : 'book-icon--circle'}`),
            'data-histre-book-id': book.book_id,
            'data-histre-book-added': isAdded ? 'added' : 'not-added',
          });
          $bookCheckWrap.appendChild($bookCheck);

          const { access } = book;
          let bookIconClass, info;
          if (access === 'public') {
            bookIconClass = isPopup ? 'globe' : 'public';
            info = 'Visible to public';
          } else if (book.shared_user_usernames.length > 0) {
            bookIconClass = isPopup ? 'share-2' : 'limited';
            info = `Shared with ${book.shared_user_usernames.length} ${book.shared_user_usernames.length === 1 ? 'person' : 'people'}`;
          } else if (access === 'org') {
            const orgText = (orgAccessLabel) ? orgAccessLabel.toLowerCase() : 'your org';
            bookIconClass = isPopup ? 'users' : 'org';
            info = `Visible to ${orgText}`;
          } else {
            bookIconClass = isPopup ? 'lock' : 'private';
          }

          const $bookIcon = $e('div', {
            class: prefix(`book-access ${isPopup ? `fe fe-${bookIconClass}` : `book-icon book-icon--${bookIconClass}`}`),
          });
          $bookCheckWrap.appendChild($bookIcon);

          const $bookName = $e('div', { class: prefix('book-name') }, noEvents, [$t(book.title)]);
          $bookCheckWrap.appendChild($bookName);

          const $bookShareableLink = $e('div', {
            class: prefix('book-icon book-icon--copy'),
            role: 'button',
            tabindex: 0,
            'aria-label': 'Copy collection link to clipboard',
            title: 'Copy collection link to clipboard',
          }, { click: copyLinkToClipboard });
          $bookCheckWrap.appendChild($bookShareableLink);

          const $bookOpenLink = $e(isPopup ? 'i' : 'div', {
            class: isPopup ? 'fe fe-link' : prefix('book-icon book-icon--view'),
            role: 'button',
            tabindex: 0,
            'aria-label': 'View collection details',
            title: 'View collection details',
          }, { click: openBookLink });
          $bookCheckWrap.appendChild($bookOpenLink);

          $bookItemWrap.appendChild($bookCheckWrap);

          const $bookInfo = $e('div', { class: prefix('book-info') }, noEvents, [$t(info)]);
          $bookItemWrap.appendChild($bookInfo);

          $recentBooksBooks.appendChild($bookItemWrap);

          function handleBookSelection(e) {
            if (
              e.target.classList.contains(prefix('book-icon--copy')) ||
              e.target.classList.contains(prefix('book-icon--copy-check')) ||
              e.target.classList.contains(isPopup ? 'fe fe-link' : prefix('book-icon--view'))
            ) {
              return;
            }

            const updatedCheckedState = $bookCheckWrap.getAttribute('aria-checked') === 'false' ? 'true' : 'false';
            $bookCheckWrap.setAttribute('aria-checked', updatedCheckedState);

            $bookCheck.classList.toggle(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
            $bookCheck.classList.toggle(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));

            if ($bookCheck.dataset.histreBookAdded === 'not-added') {
              $bookCheck.dataset.histreBookAdded = 'added';
            }

            if ($recentBooksShowMore.style.display !== 'none') {
              countSelectedBooks();
            }

            saveNoteDebounced();
            quill.focus();
          }

          function copyLinkToClipboard() {
            $recentBooksBooks.querySelectorAll(`.${prefix('book-icon--copy-check')}`).forEach((icon) => {
              icon.classList.remove(prefix('book-icon--copy-check'));
              icon.title = 'Copy collection link to clipboard';
              icon.setAttribute('aria-label', 'Copy collection link to clipboard');
              icon.classList.add(prefix('book-icon--copy'));
            });

            navigator.clipboard.writeText(book.shareable_link);
            this.classList.remove(prefix('book-icon--copy'));
            this.classList.add(prefix('book-icon--copy-check'));
            this.title = 'Link copied to clipboard';
            this.setAttribute('aria-label', 'Link copied to clipboard');
          }

          function openBookLink() {
            const bookLink = `${host}/collections/${book.book_id}/`;
            window.open(bookLink, '_blank');
          }
        });

        if (shownRecentBooks.length > maxBooksDisplayed) {
          if (isSearch) {
            $recentBooksShowMoreWithoutCount.style.display = 'block';
          } else {
            $recentBooksShowMore.style.display = 'block';
            countSelectedBooks();
          }
        }

        if (isPopup) {
          chrome.storage.local.get(['popupShowRecentBooks'], (result) => {
            if (result.popupShowRecentBooks) {
              showRecentBooks();
            } else {
              hideRecentBooks();
            }

            $recentBooks.style.display = 'block';
          });
        } else if (dest === 'sidebar') {
          chrome.storage.local.get(['sidebarShowRecentBooks'], (result) => {
            if (result.sidebarShowRecentBooks) {
              showRecentBooks();
            } else {
              hideRecentBooks();
            }

            $recentBooks.style.display = 'block';
          });
        } else {
          showRecentBooks();
          $recentBooks.style.display = 'block';
        }
      };

      function countSelectedBooks() {
        const result = $recentBooksBody.querySelectorAll(`.${isPopup ? 'fe-check-circle' : prefix('book-icon--checked')}`);
        $recentBooksCount.textContent = result.length;
      }

      function showRecentBooks() {
        $recentBooksBody.style.display = 'block';
        $recentBooksToggleIcon.classList.remove(isPopup ? 'fe-chevron-down' : prefix('book-toggle-icon--show'));
        $recentBooksToggleIcon.classList.add(isPopup ? 'fe-chevron-up' : prefix('book-toggle-icon--hide'));
        $recentBooksSearchToggle.style.display = 'block';
        $recentBooksCreateToggle.style.display = 'block';
      }

      function hideRecentBooks() {
        $recentBooksBody.style.display = 'none';
        $recentBooksToggleIcon.classList.remove(isPopup ? 'fe-chevron-up' : prefix('book-toggle-icon--hide'));
        $recentBooksToggleIcon.classList.add(isPopup ? 'fe-chevron-down' : prefix('book-toggle-icon--show'));
        $recentBooksSearchToggle.style.display = 'none';
        $recentBooksCreateToggle.style.display = 'none';
      }

      function toggleRecentBooks(e) {
        // Do not toggle collapse if user is clicking on create book, search books, etc. buttons
        if (e.target.closest('[data-histre-add-to-book-for]') !== null) {
          return;
        }

        if ($recentBooksBody.style.display === 'block' || $recentBooksBody.style.display === '') {
          hideRecentBooks();
          if (isPopup) {
            chrome.storage.local.set({ popupShowRecentBooks: false });
          } else if (dest === 'sidebar') {
            chrome.storage.local.set({ sidebarShowRecentBooks: false });
            chrome.runtime.sendMessage({ action: 'closeRecentBooks', dest: 'sidebar' });
          }
        } else {
          showRecentBooks();
          if (isPopup) {
            chrome.storage.local.set({ popupShowRecentBooks: true });
          } else if (dest === 'sidebar') {
            chrome.storage.local.set({ sidebarShowRecentBooks: true });
            chrome.runtime.sendMessage({ action: 'showRecentBooks', dest: 'sidebar' });
          }
        }
      }

      function toggleRecentBooksSearch() {
        if ($recentBooksSearch.style.display === 'block') {
          $recentBooksSearch.style.display = 'none';

          if (JSON.stringify(shownRecentBooks) !== JSON.stringify(recentBooks)) {
            shownRecentBooks = recentBooks;
            $recentBooksSearchInput.value = '';
            $recentBooksSearchText.textContent = 'Type a collection title or keywords';
            setupRecentBooks();
          }

          quill.focus();
        } else {
          $recentBooksSearch.style.display = 'block';
          $recentBooksSearchInput.focus();
        }
      }

      function toggleRecentBooksCreate() {
        if ($recentBooksCreate.style.display === 'grid') {
          $recentBooksCreate.style.display = 'none';
          $recentBooksCreateCheck.classList.remove(prefix(isPopup ? 'fe-check-circle' : 'book-icon--checked'));
          $recentBooksCreateCheck.classList.add(prefix(isPopup ? 'fe-circle' : 'book-icon--circle'));
          quill.focus();
          if ($recentBooksShowMore.style.display !== 'none') {
            countSelectedBooks();
          }
        } else {
          $recentBooksCreate.style.display = 'grid';
          $recentBooksCreateCheck.classList.remove(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
          $recentBooksCreateCheck.classList.add(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));
          $recentBooksCreateInput.focus();
          if ($recentBooksShowMore.style.display !== 'none') {
            countSelectedBooks();
          }
        }
      }

      function handleBookCreateKeydown(e) {
        if (e.key === 'Escape') {
          toggleRecentBooksCreate();
        } else if (e.key === 'Enter') {
          saveNote();
        }
      }

      function handleRecentBooksSearch() {
        const search = this.value;
        const prevShownBooks = shownRecentBooks;

        if (search === '') {
          shownRecentBooks = recentBooks;
          setupRecentBooks();
          $recentBooksSearchText.textContent = 'Type a collection title or keywords';
          return;
        }

        $recentBooksShowMore.style.display = 'none';

        shownRecentBooks = recentBooks.filter((book) => {
          return book.title.toLowerCase().includes(search.toLowerCase());
        });

        if (JSON.stringify(prevShownBooks) !== JSON.stringify(shownRecentBooks)) {
          setupRecentBooks(true); // rerender
        }

        if (shownRecentBooks.length <= 0) {
          $recentBooksSearchText.textContent = 'No collections found for your search';
        } else {
          $recentBooksSearchText.textContent = `Found ${shownRecentBooks.length} collection${shownRecentBooks.length !== 1 ? 's' : ''} for your search`;
        }
      }

      function handleSearchKeydown(e) {
        if (e.key === 'Escape') {
          toggleRecentBooksSearch();
        }
      }

      function getNewBookTitle() {
        if ($recentBooksCreate.style.display !== 'grid' || $recentBooksCreateInput.disabled === true) {
          return null;
        }
        disableNewBookActions();
        if ($recentBooksCreateInput.value === '') {
          // Throw error
          enableNewBookActions();
          return null;
        } else {
          return $recentBooksCreateInput.value.trim();
        }
      }

      function disableNewBookActions() {
        $recentBooksCreateInput.disabled = true;
        $recentBooksCreateAccept.style.visibility = 'hidden';
        $recentBooksCreateCancel.style.visibility = 'hidden';
      }

      function enableNewBookActions() {
        $recentBooksCreateInput.disabled = false;
        $recentBooksCreateAccept.style.visibility = 'visible';
        $recentBooksCreateCancel.style.visibility = 'visible';
        quill.focus();
      }

      function enableNewBook(newBookId) {
        $recentBooksCreateCheck.dataset.histreBookId = newBookId;
        $recentBooksCreateCheck.dataset.histreBookAdded = 'added';
        $recentBooksCreateInput.style.cursor = 'pointer';
        $recentBooksCreate.style.cursor = 'pointer';
        $recentBooksCreate.setAttribute('role', 'checkbox');
        $recentBooksCreate.setAttribute('aria-checked', `${$recentBooksCreateCheck.classList.contains(isPopup ? 'fe-circle' : prefix('book-icon--circle'))}`);
        $recentBooksCreate.setAttribute('tabindex', 0);
        quill.focus();
        $recentBooksCreate.addEventListener('click', () => {
          if ($recentBooksCreateCheck.classList.contains(prefix(isPopup ? 'fe-circle' : 'book-icon--circle'))) {
            $recentBooksCreateCheck.classList.remove(prefix(isPopup ? 'fe-circle' : 'book-icon--circle'));
            $recentBooksCreateCheck.classList.add(prefix(isPopup ? 'fe-check-circle' : 'book-icon--checked'));
            $recentBooksCreate.setAttribute('aria-checked', 'true');
          } else {
            $recentBooksCreateCheck.classList.remove(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));
            $recentBooksCreateCheck.classList.add(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
            $recentBooksCreate.setAttribute('aria-checked', 'false');
          }
          countSelectedBooks();
          saveNoteDebounced();
          quill.focus();
        });
      }

      function getBookIdsForAddNote() {
        const bookIdsForAddNote = [];
        const checkBookItems = $recentBooks.querySelectorAll(`.${isPopup ? 'fe-check-circle' : prefix('book-icon--checked')}`);
        Array.from(checkBookItems).forEach((checkedBookItem) => {
          const bookId = checkedBookItem.dataset.histreBookId;
          if (bookId && bookId.length > 0) {
            bookIdsForAddNote.push(bookId);
          }
        });
        return bookIdsForAddNote;
      };

      function getDeselectedBookIds() {
        let deselectedBookIds = [];
        const uncheckedBooks = $recentBooksBooks.querySelectorAll(`.${isPopup ? 'fe-circle' : prefix('book-icon--circle')}`);
        Array.from(uncheckedBooks).forEach((book) => {
          if (book.dataset.histreBookAdded !== 'not-added') {
            deselectedBookIds.push(book.dataset.histreBookId);
          }
        });
        if ($recentBooksCreateCheck.dataset.histreBookId && $recentBooksCreateCheck.classList.contains(isPopup ? 'fe-circle' : prefix('book-icon--circle'))) {
          deselectedBookIds.push($recentBooksCreateCheck.dataset.histreBookId);
        }
        return deselectedBookIds;
      };

      function reCheckBooks(bookIds) {
        bookIds.forEach((bookId) => {
          const bookCheck = $recentBooksBooks.querySelector(`.${isPopup ? 'fe-circle' : prefix('book-icon--circle')}[data-histre-book-id="${bookId}"]`);
          const bookCheckWrap = bookCheck.closest(`.${prefix('book-check-wrap')}`);

          const updatedCheckedState = bookCheckWrap.getAttribute('aria-checked') === 'false' ? 'true' : 'false';
          bookCheckWrap.setAttribute('aria-checked', updatedCheckedState);

          bookCheck.classList.toggle(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
          bookCheck.classList.toggle(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));
        });

        if ($recentBooksShowMore.style.display !== 'none') {
          countSelectedBooks();
        }
      }

      function showAllBooks() {
        [...$recentBooksBooks.children].forEach((book) => {
          book.style.display = 'block';
        });
        this.style.display = 'none';
        quill.focus();
      }

      function renderRecentBooks(newRecentBooks) {
        const prevRecentBooks = recentBooks;
        recentBooks = newRecentBooks;
        shownRecentBooks = recentBooks;
        if (JSON.stringify(prevRecentBooks) !== JSON.stringify(recentBooks)) {
          setupRecentBooks(); // rerender
        }
      };

      function updateRecentBooks(checkedIds, noteId) {
        recentBooks.forEach((book) => {
          if (checkedIds.includes(book.book_id)) {
            book.current_url_item_id = noteId;
          } else {
            if (book.current_url_item_id) {
              delete book.current_url_item_id;
            }
          }
        });

        setupRecentBooks();
      }

      setupRecentBooks();

      return {
        element: $recentBooks,
        renderRecentBooks,
        updateRecentBooks,
        showRecentBooks,
        hideRecentBooks,
        getBookIdsForAddNote,
        getDeselectedBookIds,
        reCheckBooks,
        getNewBookTitle,
        enableNewBookActions,
        enableNewBook,
      }
    }

    //----- Recent books : End -----//

    getMentionConfig() {
      return {
        allowedChars: /^[a-z0-9]*$/i,
        mentionDenotationChars: ['#'],
        isolateCharacter: true,
        minChars: 1,
        source: (searchTerm, renderList, mentionChar) => {
          let values;

          if (mentionChar === '@') {
            values = [];
          } else {
            values = this.refs.tagList;
          }

          if (searchTerm.length === 0) {
            renderList(values, searchTerm);
          } else {
            const startMatches = [];
            const anyMatches = [];
            let srch, itemval, loc;
            for (let i = 0; i < values.length; i++) {
              srch = searchTerm.toLowerCase();
              itemval = values[i].value.toLowerCase();
              loc = itemval.indexOf(srch);
              if (loc === 0) {
                startMatches.push(values[i]);
              } else if (loc > -1) {
                anyMatches.push(values[i]);
              } else {
                // not matched. skip.
              }
            }
            const matches = startMatches.concat(anyMatches);
            const maxMatches = 6;
            renderList(matches.slice(0, maxMatches), searchTerm);
          }
        },
        onSelect: (item, insertItem) => {
          const selection = this.refs.quill.getSelection(true);
          const pos = selection.index;
          let start = pos;
          let char;
          let found = false;
          while (start >= 0) {
            char = this.refs.quill.getText(start, 1);
            if (char === item.denotationChar) {
              found = true;
              break;
            }
            start -= 1;
          }
          if (found) {
            this.refs.quill.deleteText(start + 1, pos - start);
            this.refs.quill.insertText(start + 1, item.value + ' ', 'user');
            this.refs.quill.setSelection(start + item.value.length + 2, 0, 'user');
          }
        },
      }
    }

    //----- YouTube Timestamp -----//

    createYTTimestamp(tabId, tabUrl) {
      const isPopup = this.isPopup;
      const prefix = this.prefix;
      const refs = this.refs;

      const $youtubeTimestamp = $e('div', {
        id: prefix('yt-timestamp'),
        class: prefix('yt-timestamp'),
      }, { click: insertYoutubeTimestamp }, [
        isPopup ? $e('i', { class: 'fe fe-youtube' }) : $e('span', { class: prefix('yt-timeline-icon') }),
        $t('Save current time'),
      ]);

      function formatTime(duration) {
        // ~~ is substitution for Math.floor
        const hrs = ~~(duration / 3600);
        const mins = ~~((duration % 3600) / 60);
        const secs = ~~duration % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        let time = '';

        if (hrs > 0) {
          time += '' + hrs + ':' + (mins < 10 ? '0' : '');
        }

        time += '' + mins + ':' + (secs < 10 ? '0' : '');
        time += '' + secs;
        return time;
      }

      function jumpToYoutubeTimpestamp(e) {
        if (e.target.getAttribute('href')?.includes('https://www.youtube.com/watch?')) {
          e.preventDefault();
          const url = new URL(e.target.getAttribute('href'));
          const time = url.searchParams.get('t');

          if (isPopup) {
            chrome.tabs.sendMessage(tabId, { goToYoutubeTimestamp: time });
          } else {
            window.histreYoutubeTimestamp.goToYoutubeTimestamp(time);
          }
        }
      }

      async function insertYoutubeTimestamp() {
        function sendMessage(tabId, message) {
          return new Promise((resolve) => {
            chrome.tabs.sendMessage(tabId, message, (result) => {
              if (chrome.runtime.lastError) { }
              resolve(result);
            });
          });
        }

        let time;
        if (isPopup) {
          time = await sendMessage(tabId, { getYoutubeTimestamp: true });
        } else {
          time = window.histreYoutubeTimestamp.getYoutubeTimestamp();
        }
        if (typeof time !== 'number') {
          return;
        }
        const formatedTime = formatTime(time);
        const url = new URL(isPopup ? tabUrl : location.href);
        url.searchParams.set('t', time);
        const link = `<p><a href="${url}">${formatedTime}</a></p><p><br></p>`;
        let note = DOMPurify.sanitize(refs.quill.root.innerHTML);
        if (note === '<p><br></p>') {
          note = link;
        } else {
          note += link;
        }
        refs.quill.setText('', 'silent');
        refs.quill.clipboard.dangerouslyPasteHTML(
          null,
          note,
          'user',
        );
      }

      return {
        element: $youtubeTimestamp,
        jumpToYoutubeTimpestamp,
      };
    }
  }

  window.histreUtils = Utils;
})();
