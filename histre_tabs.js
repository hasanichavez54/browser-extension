// do not use console.log or helper.llog here

function initialize() {
  if (!window.histre_tabs) {
    var tabOpenerElement = document.getElementById('tab-opener');
    if (!tabOpenerElement) {
      return;
    }

    window.histre_tabs = true;
    tabOpenerElement.classList.remove('hide');

    tabOpenerElement.onclick = function () {
      items = document.querySelectorAll('.book-notes .note-title');
      urls = [];
      var arrayLength = items.length;
      for (var i = 0; i < arrayLength; i++) {
        urls.push(items[i].href);
      }
      if (chrome.runtime) {
        try {
          chrome.runtime.sendMessage({ type: 'open_tabs', urls: urls });
        } catch (e) {
          /* TODO notify user */
        }
      }
    };
  }
}

initialize();
