(() => {
  if (window.histreLoadedHighlight) {
    return true;
  }
  window.histreLoadedHighlight = true;

  // For Firefox
  if (typeof browser === 'object') { chrome = browser; }

  return false;
})();
