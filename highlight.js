(function () {
  const histreDOMHelper = window.histreDOMHelper;
  // TODO convert elements using these functions
  // const { $e, $t, noEvents } = histreDOMHelper;
  // const prefix = (classNames) => histreDOMHelper.prefix('highlight', classNames);
  const host = histreDOMHelper.getHost();
  const highlightLinkBase = host + '/highlights/';

  const MIN_HIGHLIGHT_LEN = 4;

  function disableHighlights() {
    const excludedHosts = {
      'mail.google.com': true,
      'docs.google.com': true,
    };
    return (location.hostname in excludedHosts);
  }

  const options = {
    wildcards: 'enabled',
    acrossElements: true,
    separateWordSearch: false,
    ignoreJoiners: true,
    ignorePunctuation: true,
  };

  let toastTimeout;

  const hostsWithMenuBelow = {
    'coda.io': true,
    'medium.com': true,
    'slack.com': true,
  }
  const domainName = location.hostname.split(/\./).slice(-2).join('.');
  const isMenuBelow = domainName in hostsWithMenuBelow;

  function getUserInfo(cb) {
    chrome.storage.local.get(['userUsername', 'userExternalId'], (items) => {
      if (items.userUsername && items.userExternalId) {
        return cb({
          username: items.userUsername,
          externalId: items.userExternalId,
        });
      } else {
        chrome.runtime.sendMessage({ type: 'setUserInfo' }, {}, (res) => {
          if (res.isSet) {
            getUserInfo(cb);
          } else {
            //TODO Implement some sort of error message or figure out how to disable menu on user signout
            return;
          }
        });
      }
    });
  }

  function highlight(e, cb) {
    const t = e.target;
    const color = t.id.split('-')[2];
    if (color === 'info') {
      window.open('https://histre.com/features/highlights/');
      return;
    }

    const data = cb(color);
    closeUI();

    if (!data) {
      return;
    }

    // persist highlight on the backend
    try {
      chrome.runtime.sendMessage(data, function (response) {
        if (chrome.runtime.lastError) {
          console.log(JSON.stringify(chrome.runtime.lastError));
        }
        if (response && response.usermsg) {
          // TODO: Replace below with logic to show error message to user
          console.log(response.usermsg);
        }
      });
    } catch (e) {
      // connection to background scripts lost, perhaps due to extn reload
      // clear highlights etc so the user reloads the page
      nuke_self();
    }
  }

  function highlighter(e) {
    highlight(e, (color) => {
      const sel = window.getSelection();

      if (sel && sel.rangeCount > 0) {
        const range = sel.getRangeAt(0);
        const text = range.toString().trim();
        window.getSelection().removeAllRanges();
        if (text.length < MIN_HIGHLIGHT_LEN) { return; }

        getUserInfo(({ username, externalId }) => {
          const highlightid = sha1(externalId + location.href + text).substring(0, 8);
          render_highlights([[text, color, highlightid]]);
          copyLinkToClipboard(username, highlightid);
        });

        return {
          type: 'save_highlight',
          highlight: {
            color: color,
            text: text,
            url: location.href,
            title: document.title,
            tweet: color === 'tweet',
          },
        };
      }
    });
  }

  function editor(e) {
    const highlightid = document.getElementById('histre-highlight-edit-menu').dataset.histreHighlightId;

    highlight(e, (color) => {
      if (color === 'delete') {
        delete_highlight(highlightid);

        return {
          type: 'delete_highlight',
          highlight: {
            id: highlightid,
          }
        };
      } else if (color === 'copy') {
        getUserInfo(({ username }) => {
          copyLinkToClipboard(username, highlightid);
        });
      } else {
        const marks = [...document.getElementsByClassName(`histre-highlight-id-${highlightid}`)];
        const markColorClass = [...marks[0].classList].filter((className) => className.includes('histre-highlight-color-'))[0];
        marks.forEach((mark) => {
          mark.classList.replace(
            markColorClass,
            `histre-highlight-color-${color}`
          )
        });

        return {
          type: 'update_highlight',
          highlight: {
            id: highlightid,
            color: color,
          },
        };
      }
    });
  }

  function clearAllMarks() {
    const context = document.querySelector('body');
    const instance = new Mark(context);
    instance.unmark();
  }

  function nuke_self() {
    window.histreLoadedHighlight = false;
    clearAllMarks();
    chrome.runtime.onMessage.removeListener(trigger_highlights);

    // TODO : mouseup and mousedown still fire! But it is harmless, so it can be fixed later
    document.body.removeEventListener('mouseup', handleMouseUp, false);
    window.removeEventListener('mousedown', handleMouseDown, false);
  }

  function render_highlights(highlights) {
    const context = document.querySelector('body');
    const instance = new Mark(context);

    highlights.sort(function (a, b) {
      // prevent highlights of substrings being hidden by superstring highlight
      return b[0].length - a[0].length;
    });

    for (let i = 0; i < highlights.length; i++) {
      const [txt, color, highlightid] = highlights[i];
      if (typeof color === 'undefined') {
        color = 'yellow';
      }

      instance.mark(txt, {
        ...options,
        className: `histre-highlight-color-${color} histre-highlight-id-${highlightid}`,
      });
    }
  }

  function delete_highlight(highlightid) {
    if (highlightid == '') {
      return;
    }
    const context = document.querySelector('body');
    const instance = new Mark(context);
    instance.unmark({
      className: `histre-highlight-id-${highlightid}`,
    });
  }

  function trigger_highlights(page_highlights) {
    if (page_highlights.action === 'show_highlights' && page_highlights.data) {
      const h = page_highlights.data.map((x) => {
        return [x.text, x.color || 'blue', x.highlight_id];
      });
      render_highlights(h);
    } else if (page_highlights.action === 'hide_highlights') {
      clearAllMarks();
    }
  }

  function copyLinkToClipboard(username, highlightid) {
    clearTimeout(toastTimeout);
    navigator.clipboard.writeText(highlightLinkBase + username + '/' + highlightid);

    const toast = document.getElementById('histre-highlight-toast');
    toast.classList.add('show');
    toastTimeout = setTimeout(() => {
      toast.classList.remove('show');
    }, 3000);
  }

  function openUI(hltMenu, coordinates) {
    hltMenu.classList.add('histre-highlight-menu--active');
    if (isMenuBelow) {
      hltMenu.style.top = `${
        coordinates.bottom +
        8 +
        window.scrollY
      }px`;
    } else {
      hltMenu.style.top = `${
        coordinates.top -
        hltMenu.getBoundingClientRect().height -
        8 +
        window.scrollY
      }px`;
    }

    hltMenu.style.left = `${
      coordinates.left +
      coordinates.width / 2 -
      hltMenu.getBoundingClientRect().width / 2
    }px`;
  }

  function closeUI() {
    document
      .getElementById('histre-highlight-menu')
      ?.classList.remove('histre-highlight-menu--active');

    document
      .getElementById('histre-highlight-edit-menu')
      ?.classList.remove('histre-highlight-menu--active');
  }

  function handleMouseUp(e) {
    if (
      !window.getSelection().toString() &&
      e.target.nodeName.toLowerCase() === 'mark'
    ) {
      const editHighlightMenu = document.getElementById('histre-highlight-edit-menu');
      const markCoordinates = e.target.getBoundingClientRect();
      // Set highlight reference for editing
      const markClasses = [...e.target.classList];
      editHighlightMenu.dataset.histreHighlightId = markClasses
        .filter((className) => className.includes('histre-highlight-id-'))
        .map((className) => className.replace('histre-highlight-id-', ''))
        [0];

      openUI(editHighlightMenu, markCoordinates);
    } else if (
      window.getSelection().toString().trim().length >= MIN_HIGHLIGHT_LEN &&
      e.target.tagName !== 'BUTTON' &&
      (
        e.target.nodeName.toLowerCase() === 'mark' ||
        (
          !(e.target.className instanceof SVGAnimatedString) &&
          !e.target.className.includes('histre-highlight')
        )
      )
    ) {
      const selectedText = window.getSelection();
      if (selectedText && selectedText.rangeCount > 0) {
        const highlightMenu = document.getElementById('histre-highlight-menu');
        const textCoordinates = selectedText
          .getRangeAt(0)
          .getBoundingClientRect();

        openUI(highlightMenu, textCoordinates);
      }
    }
  }

  function handleMouseDown(e) {
    if (e.target.nodeName.toLowerCase() === 'mark') {
      // no op
      // if the target is mark, don't do e.preventDefault() from below
      // this is so users can select the highlighted text
    } else if (
      e.target &&
      e.target.className &&
      e.target.className.includes &&
      e.target.className.includes('histre-highlight')
    ) {
      // clicking on the highlight ui shouldn't remove the selection before we can grab it
      e.preventDefault();
    } else {
      closeUI();
    }
  }

  function handleSelectionChange() {
    // handle the case where user clicks on selected text to unselect it
    // we need this because, when the user clicks on the current selection,
    // the selection is cleared AFTER the mouseup event
    if (!window.getSelection().toString()) {
      closeUI();
    }
  }

  function setUpHighlightUI() {
    document.body.addEventListener('mouseup', handleMouseUp, false);
    window.addEventListener('mousedown', handleMouseDown, false);

    // handle the case where user clicks on selected text to unselect it
    document.onselectionchange = handleSelectionChange;

    const highlighterButtons = document.querySelectorAll('#histre-highlight-menu .histre-highlight-button');
    for (var i = 0; i < highlighterButtons.length; i++) {
      highlighterButtons[i].addEventListener('click', highlighter);
    }

    const editorButtons = document.querySelectorAll('#histre-highlight-edit-menu .histre-highlight-button');
    for (let i = 0; i < editorButtons.length; i++) {
      editorButtons[i].addEventListener('click', editor);
    }
  }

  function createHighlightUI(colors, type = 'default') {
    //? To prevent creating duplicated elements
    if (type === 'default') {
      while (document.getElementById('histre-highlight-menu')) {
        document.getElementById('histre-highlight-menu').remove();
      }
    } else if (type === 'edit') {
      while (document.getElementById('histre-highlight-edit-menu')) {
        document.getElementById('histre-highlight-edit-menu').remove();
      }
    }

    const menuInner = document.createElement('div');
    menuInner.setAttribute('class', 'histre-highlight-menu-inner');

    let btn;
    colors.forEach((color) => {
      btn = document.createElement('div');
      btn.setAttribute('id', `histre-highlight-${color}`);
      btn.setAttribute('class', 'histre-highlight-button');
      menuInner.appendChild(btn);
    });

    const menuArrowClip = document.createElement('div');
    menuArrowClip.setAttribute('class', 'histre-highlight-menu-arrowClip');
    const menuArrow = document.createElement('span');
    menuArrow.setAttribute('class', 'histre-highlight-menu-arrow');
    menuArrowClip.appendChild(menuArrow);

    const hi = document.createElement('div');
    hi.appendChild(menuInner);
    hi.appendChild(menuArrowClip);
    hi.setAttribute(
      'class',
      `histre-highlight-menu ${isMenuBelow && 'histre-highlight-menu--bottom'} ${type === 'edit' && 'histre-highlight-menu--edit'}`
    );
    hi.setAttribute(
      'id',
      `${type === 'edit' ? 'histre-highlight-edit-menu' : 'histre-highlight-menu'}`
    );
    document.body.appendChild(hi);
  }

  function insertHighlightCSS() {
    const style = document.createElement('link');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    style.href = chrome.runtime.getURL('highlight.css');
    (document.head || document.documentElement).appendChild(style);
  }

  function createToast() {
    //? To prevent creating duplicated elements
    while (document.getElementById('histre-highlight-toast')) {
      document.getElementById('histre-highlight-toast').remove();
    }

    const toast = document.createElement('div');
    toast.setAttribute('id', 'histre-highlight-toast');
    toast.innerText = 'Highlight link copied to clipboard';
    document.body.appendChild(toast);
  }

  const colorList = [
    'yellow',
    'orange',
    'green',
    'blue',
    'purple',
    'red',
  ];

  if (!disableHighlights()) {
    // Highlight UI
    createHighlightUI([
      ...colorList,
      // 'tweet',
      'info',
    ]);
    // Edit UI
    createHighlightUI([
      ...colorList,
      'copy',
      'delete',
      'info',
    ], 'edit');
    setUpHighlightUI();
    insertHighlightCSS();

    createToast();

    chrome.runtime.onMessage.addListener(trigger_highlights);
  }

  return true;
})();
