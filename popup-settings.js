const settingsUrl = `${apiBase}/settings/`;

const settingsState = {};

const loadSettings = () => {
  chrome.permissions.contains(
    {
      origins: ['*://*/*'],
    },
    (enabled) => {
      settingsState.enable_permissions = !!enabled;
      $('#checkbox-enhance').prop('checked', settingsState.enable_permissions);
      chrome.storage.local.get(
        ['log_account', 'log_browser', 'enable_highlights'],
        (result) => {
          let log_account = result.log_account;
          let log_browser = result.log_browser;
          if (typeof log_account === 'undefined') {
            log_account = true;
          }
          if (typeof log_browser === 'undefined') {
            log_browser = true;
          }
          settingsState.log_account = !!log_account;
          settingsState.log_browser = !!log_browser;
          settingsState.enable_highlights = result.enable_highlights;
          rerenderSettings();
        }
      );
    }
  );
};

const rerenderSettings = () => {
  $('#checkbox-enhance').prop('checked', settingsState.enable_permissions);
  $('#checkbox-highlights').prop('checked', settingsState.enable_highlights);
  $('#checkbox-all-browsers').prop('checked', settingsState.log_account);
  $('#checkbox-this-browser').prop(
    'checked',
    settingsState.log_browser && settingsState.log_account
  );
  loggingmsg();
};

const updateStorage = (key, val, cb) => {
  if (!cb) {
    cb = loadSettings;
  }
  const kv = {};
  kv[key] = val;
  chrome.storage.local.set(kv, cb);
};

const loggingmsg = () => {
  let msg = '';
  if (!settingsState.log_account) {
    msg = 'disabled on all browsers';
  } else if (settingsState.log_account && !settingsState.log_browser) {
    msg = 'disabled on this browser';
  } else {
    msg = 'enabled';
  }
  $('#logging-status').text(`Status: Logging ${msg}`);
};

const enhanceWebsites = () => {
  chrome.permissions.request(
    {
      origins: ['*://*/*'],
      permissions: ['bookmarks', 'history'],
    },
    function (granted) {
      updateStorage('enable_permissions', !!granted);
    }
  );
};

const setSettingsMsg = (msg) => {
  if (msg === 'loading') {
    $('#account-switch-loading').show();
    $('#account-switch-errored').hide();
  }
  if (msg === 'error') {
    $('#account-switch-loading').hide();
    $('#account-switch-errored').show();
  }
  if (msg === 'success') {
    $('#account-switch-errored').hide();
    $('#account-switch-loading').hide();
  }
};

const updateHistreSettings = (enable) => {
  setSettingsMsg('loading');
  const data = { history: enable };
  fetch(settingsUrl, helper.setRequestParams('POST', data))
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      const { settings: { history }, error, errcode } = result;
      if (error) {
        if (
          errcode === helper.HttpCodes.unauthorized ||
          errcode === helper.HttpCodes.forbidden
        ) {
          helper.handleAuthErrorsOnce(() => {
            setSettingsMsg('error');
            return;
          });
        } else {
          throw new Error('Error updating settings');
        }
      } else {
        setSettingsMsg('success');
        updateStorage('log_account', history);
        if (history) {
          updateStorage('log_browser', true);
        }
      }
    })
    .catch((error) => {
      setSettingsMsg('error');
    });
};

const updateHighlights = (enable) => {
  if (enable) {
    chrome.permissions.request(
      {
        origins: ['*://*/*'],
      },
      (enabled) => {
        settingsState.enable_highlights = enabled;
        rerenderSettings();
        updateStorage('enable_highlights', enabled); // background.js listens to this
      }
    );
  } else {
    updateStorage('enable_highlights', false); // background.js listens to this
  }
};

const handleSettingsClicks = () => {
  $('#checkbox-enhance').on('click', function () {
    if ($(this).is(':checked')) {
      enhanceWebsites();
    }
  });
  $('#checkbox-highlights').on('click', function () {
    const checked = $(this).is(':checked');
    updateHighlights(checked);
  });
  $('#checkbox-all-browsers').on('click', function () {
    const checked = $(this).is(':checked');
    updateHistreSettings(checked);
  });
  $('#checkbox-this-browser').on('click', function () {
    const checked = $(this).is(':checked');
    updateStorage('log_browser', checked);
  });
};

$(document).ready(() => {
  loadSettings();
  handleSettingsClicks();
});
