(function () {
  const hnvotesUrl = `${apiBase}/hnvotes/`;

  function addHNContentScript(tab) {
    chrome.permissions.contains(
      {
        origins: ['https://news.ycombinator.com/'],
      },
      function (result) {
        if (result) {
          tab.id,
            chrome.tabs.executeScript(
              {
                file: 'hnvotes.js',
              },
              function () {
                const requestUrl = `${hnvotesUrl}?url=${encodeURIComponent(tab.url)}`;
                helper.doGet(requestUrl, {}, (data) => {
                  chrome.tabs.sendMessage(
                    tab.id,
                    {
                      action: 'show_upvoted',
                      data: data.data,
                    },
                    (response) => {
                      if (chrome.runtime.lastError) {
                        // no op
                      } else {
                        // no op
                      }
                    }
                  );
                });
              }
            );
          if (chrome.runtime.lastError) {
          }
        } else {
          // we don't have the permission to do that
        }
      }
    );
  }

  function tabUpdatedHandler(tabId, changes, tab) {
    if (
      tab &&
      tab.url &&
      tab.url.indexOf('http') === 0 &&
      changes.status === 'complete'
    ) {
      if (tab.url.indexOf('news.ycombinator.com') != -1) {
        addHNContentScript(tab);
      }
    }
  }

  chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
})();
