function previousElementSibling(element) {
  if (element.previousElementSibling !== 'undefined') {
    return element.previousElementSibling;
  } else {
    while ((element = element.previousSibling)) {
      if (element.nodeType === 1) {
        return element;
      }
    }
  }
}
function cssPath2(element) {
  if (!(element instanceof HTMLElement)) {
    return false;
  }
  var path = [];
  while (element.nodeType === Node.ELEMENT_NODE) {
    var selector = element.nodeName;
    if (element.id) {
      selector += '#' + element.id;
    } else {
      var sibling = element;
      var siblingSelectors = [];
      while (sibling !== null && sibling.nodeType === Node.ELEMENT_NODE) {
        siblingSelectors.unshift(sibling.nodeName);
        sibling = previousElementSibling(sibling);
      }
      if (siblingSelectors[0] !== 'HTML') {
        siblingSelectors[0] = siblingSelectors[0] + ':first-child';
      }
      selector = siblingSelectors.join(' + ');
    }
    path.unshift(selector);
    element = element.parentNode;
  }
  return path.join(' > ');
}

function cssPath(element) {
  if (!element) {
    return '';
  }
  if (!(element instanceof HTMLElement)) {
    element = element.parentNode;
  }
  return cssPath2(element);
}
{
  let ranges = [];
  let sel = window.getSelection();
  for (let i = 0; i < sel.rangeCount; i++) {
    var range = sel.getRangeAt(i);
    ranges[i] = {
      startOffset: range.startOffset,
      endOffset: range.endOffset,
      startElem: cssPath(range.startContainer),
      commonElem: cssPath(range.commonAncestorContainer),
      endElem: cssPath(range.endContainer),
    };
  }
}
