// do not use console.log or helper.llog here

// Content script with functions/utilities to extract structured data from page

(function () {
  if (window.histre_extractable_has_run) {
    return true;
  }
  window.histre_extractable_has_run = true;

  // Some helper function and utilities
  const NOT_ON_EXTRACTABLE_PAGE = 'Not on extractable page';
  const NUMBER_RE = /\d{1,3}(,\d{3})*(\.\d{1,2})?/;

  const getNumberValue = (text) => {
    const result = text.match(NUMBER_RE);
    return (result) ? result[0] : null;
  };

  const parseNumberValue = (value) => {
    if (typeof value !== 'string' || !value.trim() || !value.match(NUMBER_RE)) {
      if (typeof value === 'number') {
        return value;
      }
      return null;
    }
    const val = value.trim().replace(/[,\s]/g, '');
    return (val.includes('.')) ? parseFloat(val) : parseInt(val);
  };

  const checkIfValuesAreEmpty = (data) => {
    let isEmpty = true;
    for (const [key, value] of Object.entries(data)) {
      if (value != null) { isEmpty = false };
    }
    return isEmpty;
  }

  // Define handler function for each website/domain here
  // Make sure the handler function returns an object with the following fields:
  // `data` - the extracted data (use snake_case for keys in `data`)
  // `errorMsg` - error message to show if extracted data is null

  const extractAirbnbListingData = () => {
    let data = null;
    let errorMsg = null;

    try {
      if (!window.location.pathname.startsWith('/rooms/')) {
        throw new Error(NOT_ON_EXTRACTABLE_PAGE);
      }

      const listingTitle = document.querySelector('h1')?.textContent;

      let price_per_night = null;
      let review_stars = null;
      const priceDataSection = document.querySelector(
        '[data-section-id="BOOK_IT_SIDEBAR"]'
      )?.textContent;
      if (priceDataSection) {
        const pricePerNightMatch = priceDataSection.match(/^(.+?)\sper night/);
        price_per_night =
          (pricePerNightMatch.length > 1) ? pricePerNightMatch[1] : null;

        if (price_per_night?.includes('/')) {
          price_per_night = price_per_night.split('/')[0].trim();
        }

        const reviewStarsMatch = priceDataSection.match(
          /night(\d(\.\d{1,2})?)\(/
        );
        review_stars = (reviewStarsMatch) ? parseNumberValue(reviewStarsMatch[1]) : null;
      }

      let location = null;
      let reviews = null;
      const titleSectionButtons = document.querySelectorAll('[data-section-id="TITLE_DEFAULT"] button');
      if (titleSectionButtons) {
        Array.from(titleSectionButtons).forEach((btn) => {
          const btnText = btn.textContent;
          if (btnText.toLowerCase().includes('review')) {
            reviews = parseNumberValue(getNumberValue(btnText));
          } else if(btnText.split(',').length >= 2) {
            location = btnText;
          }
        });
      }

      const listingDetails = {
        beds: null,
        bedrooms: null,
        guests: null,
        baths: null,
      };
      const listingDetailsSection = document.querySelector(
        '[data-section-id="OVERVIEW_DEFAULT"]'
      )?.textContent;
      if (listingDetailsSection) {
        const listingDetailsKeywords = Object.keys(listingDetails);
        const listingDetailsParts = listingDetailsSection.split(',');
        if (listingDetailsParts.length > 0) {
          listingDetailsParts.forEach((item) => {
            const itemValue = getNumberValue(item);
            const itemParts = item.split(/\s+/);
            let itemKey = itemParts[itemParts.length - 1].toLowerCase();
            itemKey = (itemKey.endsWith('s')) ? itemKey : `${itemKey}s`;
            if (itemValue && listingDetailsKeywords.includes(itemKey)) {
              listingDetails[itemKey] = parseNumberValue(itemValue);
            }
          });
        }
      }

      let superhost = null;
      const titleSectionText = document.querySelector(
        '[data-section-id="TITLE_DEFAULT"]'
      )?.textContent;
      if (titleSectionText) {
        superhost = titleSectionText.toLowerCase().includes('superhost');
      }

      data = {
        listing: listingTitle,
        beds: listingDetails.beds,
        bedrooms: listingDetails.bedrooms,
        guests: listingDetails.guests,
        baths: listingDetails.baths,
        price: price_per_night,
        rating: review_stars,
        reviews,
        superhost,
        location,
      };

      const isEmpty = checkIfValuesAreEmpty(data);
      if (isEmpty) {
        data = null;
        throw new Error('All data values are empty.');
      }
    } catch (error) {
      if (error.message !== NOT_ON_EXTRACTABLE_PAGE) {
        errorMsg = 'No Airbnb listing data found.';
      }
    }

    return { data, errorMsg };
  };

  const extractTwitterProfileData = () => {
    let data = null;
    let errorMsg = null;

    try {
      if (document.querySelector('meta[property="og:type"]').getAttribute('content') !== 'profile') {
        throw new Error(NOT_ON_EXTRACTABLE_PAGE);
      }

      let name = '';
      const title = document.querySelector('title')?.textContent;
      if (title) {
        const title_clean = title.replace('/ Twitter', '').trim();
        name = title_clean;
        const title_parts = title_clean.split(' (');
        if (title_parts.length >= 2) {
          name = title_parts[0].trim();
        }
        name = name.replace(/^\(\d+\)/, '').trim();
      }

      let username = '';
      const pathName = window.location.pathname;
      if (pathName) {
        username = pathName.replace('/', '').trim();
      }

      const bio = document.querySelector(
        '[data-testid="UserDescription"]'
      ).textContent;

      let following = document.querySelector('a[href$="/following"]');
      if (following) {
        const gMatches = following.textContent.match(/([0-9,.KM]+) Following/i);
        following = (gMatches && gMatches.length > 0) ? gMatches[1] : null;
      }

      let followers = document.querySelector('a[href$="/followers"]');
      if (followers) {
        const rMatches = followers.textContent.match(/([0-9,.KM]+) Followers/i);
        followers = (rMatches && rMatches.length > 0) ? rMatches[1] : null;
      }

      data = { name, username, bio, following, followers };

      const isEmpty = checkIfValuesAreEmpty(data);
      if (isEmpty) {
        data = null;
        throw new Error('All data values are empty.');
      }
    } catch (error) {
      if (error.message !== NOT_ON_EXTRACTABLE_PAGE) {
        // Show this error only if on profile page and no data was found
        errorMsg = 'No Twitter profile data found.';
      }
    }

    return { data, errorMsg };
  };

  // Extension message event listener

  chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if (!msg.extractable) {
      return null;
    }

    const url = new URL(msg.extractable);
    const hostname = url.hostname;

    // Add condition for each website/domain here

    if (hostname.endsWith('twitter.com')) {
      const result = extractTwitterProfileData();
      sendResponse({ result });
    } else if (hostname.endsWith('airbnb.com')) {
      const result = extractAirbnbListingData();
      sendResponse({ result });
    }

    return true;
  });
})();
