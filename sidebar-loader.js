(() => {
  if (window.histreLoadedSidebar) {
    return true;
  }
  window.histreLoadedSidebar = true;

  // For Firefox
  if (typeof browser === 'object') { chrome = browser; }

  return false;
})();
