async function runStorageMigrations(previousVersion, cb) {
  const [n0, n1, n2] = previousVersion.split('.');

  //TODO create module for async calls
  function getLocalStorage(keys = null) {
    return new Promise((resolve) => {
      chrome.storage.local.get(keys, (result) => {
        resolve(result);
      });
    });
  }

  function setLocalStorage(keys = {}) {
    return new Promise((resolve) => {
      chrome.storage.local.set(keys, (result) => {
        resolve(result);
      });
    });
  }

  function removeLocalStorage(keys = []) {
    return new Promise((resolve) => {
      chrome.storage.local.remove(keys, () => {
        resolve();
      });
    });
  }

  function hasPermissions(permissions = {}) {
    return new Promise((resolve) => {
      chrome.permissions.contains(permissions, (enabled) => {
        resolve(enabled);
      });
    });
  }

  if (n0 <= 3 && n1 <= 14 && n2 <= 11) {
    const {
      enable_permissions,
      enhance_websites,
      enable_highlights,
      log_browser,
      log_account,
      posted_history,
      posted_bookmarks_v2,
      user_username,
      user_external_id,
      show_recent_books,
    } = await getLocalStorage();
    const storageSet = {};
    const storageRemove = [];

    if (typeof enable_permissions !== 'undefined') {
      storageSet.enablePermissions = enable_permissions;
      storageSet.featuresEnhanceWebsites = enhance_websites;
      storageRemove.push('enable_permissions');
    } else {
      const enabled = await hasPermissions({
        origins: ['*://*/*'],
        permissions: ['bookmarks', 'history'],
      });

      if (enabled) {
        storageSet.enablePermissions = true;
        storageSet.featuresEnhanceWebsites = true;
      }
    }

    if (typeof enable_highlights !== 'undefined') {
      storageSet.featuresHighlights = enable_highlights;
      storageRemove.push('enable_highlights');
    }

    if (typeof log_browser !== 'undefined') {
      storageSet.featuresHistory = log_browser;
      storageRemove.push('log_browser');
    }

    if (typeof log_account !== 'undefined') {
      storageSet.userLogHistory = log_account;
      storageRemove.push('log_account');
    }

    if (typeof posted_history !== 'undefined') {
      storageSet.postedHistory = posted_history;
      storageRemove.push('posted_history');
    }

    if (typeof posted_bookmarks_v2 !== 'undefined') {
      storageSet.postedBookmarksV2 = posted_bookmarks_v2;
      storageRemove.push('posted_bookmarks_v2');
    }

    if (typeof user_username !== 'undefined') {
      storageSet.userUsername = user_username;
      storageRemove.push('user_username');
    }

    if (typeof user_external_id !== 'undefined') {
      storageSet.userExternalId = user_external_id;
      storageRemove.push('user_external_id');
    }

    if (typeof show_recent_books !== 'undefined') {
      storageSet.popupShowRecentBooks = show_recent_books;
      storageRemove.push('show_recent_books');
    }

    await setLocalStorage(storageSet);
    await removeLocalStorage(storageRemove);
  }

  if (n0 <= 3 && n1 <= 14 && n2 <= 14) {
    const enabled = await hasPermissions({ origins: ['*://*/*'] });

    if (enabled) {
      await setLocalStorage({ featuresInline: true });
    }
  }

  if (cb) { cb(); }
}
