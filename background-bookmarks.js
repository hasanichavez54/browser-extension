// Bookmarks-related background script logic
let runBookmarkImport, bookmarksOnCreatedHandler, bookmarksOnDeletedHandler;

(function() {
  const bookmarksUrl = `${apiBase}/bookmarks/`;

  let builtInBookmarksFolderNames = ['bookmarks bar', 'other bookmarks'];
  if (isFirefox) {
    builtInBookmarksFolderNames = [
      'bookmarks menu',
      'other bookmarks',
      'bookmarks toolbar',
      'mozilla firefox',
      'recent tags',
    ];
  }

  function validateBookmarkFolderName(folderName) {
    if (!folderName || folderName.length === 0) { // folder name is empty
      return false;
    }
    // Check if folderName is a built-in foldern
    const fldrName = folderName.toLowerCase();
    return !builtInBookmarksFolderNames.includes(fldrName);
  }

  function handleUnexpectedBookmarkImportErrors(errorObj) {
    // Run when the error causes bulk process to stop,
    // ensures that 'error' state is set
    chrome.storage.local.set({ postedBookmarksV2: 'error' }, () => {
      helper.llog(`Unexpected error occurred during bulk bookmark save: ${errorObj.message}`);
    });
  }

  function processBookmarkNode(bookmarkNode, parentFolders) {
    if (!parentFolders) {
      parentFolders = [];
    }
    const data = [];
    if (bookmarkNode.url) {  // Include only notes that are bookmarks not folders
      const bookmarkItem = {
        title: bookmarkNode.title,
        url: bookmarkNode.url,
        datetime: bookmarkNode.dateAdded,
      };
      if (parentFolders.length > 0) {
        bookmarkItem['folders'] = parentFolders;
      }
      data.push(bookmarkItem);
    }
    if (bookmarkNode.children) {
      const parents = parentFolders.map((folderName) => folderName);
      if (validateBookmarkFolderName(bookmarkNode.title)) {
        parents.push(bookmarkNode.title);
      }
      for (let i = 0; i < bookmarkNode.children.length; i++) {
        const newitems = processBookmarkNode(bookmarkNode.children[i], parents);
        if (newitems && newitems.length) {
          data.push(...newitems);
        }
      }
    }
    return data;
  }

  function doPostBookmarksData(data) {
    const payload = (Array.isArray(data)) ? { data } : { data: [data] };
    helper.doPost(bookmarksUrl, payload, function (result) {
      helper.llog('bookmarks create sent. result:' + JSON.stringify(result));
    });
  }

  function doPostBulkBookmarksData(bookmarksData, errorCount) {
    errorCount = errorCount || 0;
    if (!bookmarksData.length) {
      const postedBookmarksStatus = (errorCount == 0) ? 'finished' : 'error';
      chrome.storage.local.set({ postedBookmarksV2: postedBookmarksStatus }, () => {
        const msg = (postedBookmarksStatus === 'finished')?
          'Successfully saved all bookmarks.' :
          'Some errors encountered when saving bookmarks';
        helper.llog(msg);
      });
      return;
    }
    const batchSize = 500;
    const payload = { data: bookmarksData.splice(0, batchSize) }
    helper.doPost(bookmarksUrl, payload, function (result) {
      const { error, status } = result;
      if (error) {
        helper.llog(`Error on bulk bookmark save: ${status}`);
        errorCount += 1;
      } else {
        helper.llog(`Saved current batch of ${payload.data.length} bookmarks`);
      }
      doPostBulkBookmarksData(bookmarksData, errorCount);
    }, handleUnexpectedBookmarkImportErrors);
  }

  function postBookmarks() {
    chrome.bookmarks.getTree(function (bookmarkTreeNodes) {
      let data = [];
      for (let i = 0; i < bookmarkTreeNodes.length; i++) {
        const newitems = processBookmarkNode(bookmarkTreeNodes[i]);
        if (newitems && newitems.length) {
          data.push(...newitems);
        }
      }
      helper.llog(data);
      doPostBulkBookmarksData(data);
    });
  }

  function postBookmarkItemWithFolderInfo(bookmarkItemData, parentId) {
    chrome.bookmarks.get(parentId, function(results) {
      if (results.length) {
        const parent = results[0];
        bookmarkItemData.folders = bookmarkItemData.folders || [];
        if (validateBookmarkFolderName(parent.title)) {
          bookmarkItemData.folders.push(parent.title);
        }

        if (parent.parentId) {
          postBookmarkItemWithFolderInfo(bookmarkItemData, parent.parentId);
        } else {
          doPostBookmarksData(bookmarkItemData);
        }
      }
    });
  }

  bookmarksOnCreatedHandler = function(id, bookmark) {
    helper.llog("in bookmarksOnCreatedHandler");
    if (!bookmark.url) {
      // object is a folder
      return;
    }

    const data = {
      url: bookmark.url,
      title: bookmark.title,
      datetime: bookmark.dateAdded,
    };

    if (bookmark.parentId) {
      // Handle case when created bookmark is added to a folder
      postBookmarkItemWithFolderInfo(data, bookmark.parentId);
    } else {
      doPostBookmarksData(data);
    }
  };

  bookmarksOnDeletedHandler = function(id, removeInfo) {
    const payload = {
      url: removeInfo.node.url,
    };

    helper.doDelete(bookmarksUrl, payload, function (result) {
      helper.llog('bookmarks del sent' + result);
    });
  };

  function importAllBookmarks() {
    chrome.permissions.contains(
      {
        permissions: ['bookmarks'],
      },
      function (result) {
        if (result) {
          chrome.storage.local.set({ postedBookmarksV2: 'started' }, () => {
            postBookmarks();
          });
        } else {
          // we don't have the permission to do that
        }
      }
    );
  }

  runBookmarkImport = function() {
    chrome.storage.local.get(
      [
        'enablePermissions',
        'postedBookmarksV2',
      ],
      (result) => {
        const { enablePermissions, postedBookmarksV2 } = result;
        if (
          enablePermissions &&
          (
            !postedBookmarksV2 ||
            postedBookmarksV2 === 'error'
          )
          ) {
          importAllBookmarks();
        }
      }
    );
  };
})();
