(() => {
  if (typeof window.histreYoutubeTimestamp === 'object') {
    return;
  }

  function getVideo() {
    const videos = document.querySelectorAll('video');
    let video;
    // Check to see if there's multiple videos on a page
    if (videos.length > 1) {
      for (let i; i < videos.length; i++) {
        if (videos.classList.contains('html5-main-video')) {
          video = videos[i];
          break;
        }
        // Check if video is playing
        if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
          video = videos[i];
          break;
        }
      }
      // Failsafe because of the yt layout, main video should be first
      if (typeof video === 'undefined') {
        video = videos[0]
      }
    } else if (videos.length === 1) {
      video = videos[0];
    } else {
      video = null;
    }
    return video;
  }

  function getYoutubeTimestamp() {
    const video = getVideo();
    if (!video) {
      return null;
    }
    const time = Math.floor(video.currentTime);
    return time;
  }

  function goToYoutubeTimestamp(time) {
    const video = getVideo();
    if (!video) {
      return null;
    }
    video.currentTime = time;
    return;
  }

  window.histreYoutubeTimestamp = {
    getYoutubeTimestamp,
    goToYoutubeTimestamp,
  }

  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.getYoutubeTimestamp === true) {
      const time = getYoutubeTimestamp();
      sendResponse(time);
      return true;
    }

    if (message.goToYoutubeTimestamp) {
      goToYoutubeTimestamp(message.goToYoutubeTimestamp);
      return;
    }
  });
})();
