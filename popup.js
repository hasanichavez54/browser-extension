const bookSearchUrl = `${apiBase}/collections/search/`;
const booksUrl = `${apiBase}/collections/`;
const noteUrl = `${apiBase}/note/`;
const tagUrl = `${apiBase}/tag/`;
const tabSaveUrl = `${apiBase}/collections/save_tabs_or_windows/`;
const removeNoteUrl = `${apiBase}/collections/remove_note/`;
const loginPageUrl = `${host}/accounts/login/`;
const notebooksPageUrl = `${host}/collections/`;
const contentAddressableApiUrl = `${apiBase}/content_addressable_mapping/`;
const extractUrl = `${apiBase}/extract/`;

const utilsRefs = {
  quill: null,
  saveNote: null,
  saveNoteDebounced: null,
  tagList: null,
};
const utils = new window.histreUtils('popup', utilsRefs);
const dataTable = utils.createExtractedDataTable();

let pageUrl = null;
let pageTitle = null;
let currentTabId = null;
let noteLog = '';
let quill;
let enableSettings = true;
let disableNav = false;

let contentAddressableMapping = null;
let isAddressableUrl = false;

let recentBooks = null;

if (isDevMode) {
  document.querySelectorAll('a[href^="https://histre.com').forEach((link) => {
    const path = new URL(link.getAttribute('href')).pathname;
    const devLink = new URL(path, host);

    link.setAttribute('href', devLink);
  });
}

DOMPurify.addHook('afterSanitizeAttributes', (node) => {
  // allow target="_blank"
  if ('target' in node) {
    node.setAttribute('target', '_blank');
    node.setAttribute('rel', 'noopener noreferrer');
  }
});

const isAuthErrorStatus = (statusCode) => {
  return (
    statusCode === helper.HttpCodes.unauthorized ||
    statusCode === helper.HttpCodes.forbidden
  );
};

const handleAuthError = () => {
  helper.handleAuthErrorsOnce(() => {
    // Callback to run if login page cannot be shown for the meantime
    showAuthError();
  });
};

const showNoteSetupErrorMsg = (errcode, errmsg) => {
  let reason = 'An error occurred';
  if (errmsg === helper.errmsgs.urlMissing) {
    reason = 'Page URL is missing';
  } else if (errmsg === helper.errmsgs.urlTooLong) {
    reason = `URL over ${helper.MAX_URL_LEN} chars`;
  }
  showErrorMessage(`Cannot take notes on this page: ${reason}.`);
};

const debounce = (func, wait, immediate) => {
  // https://davidwalsh.name/javascript-debounce-function
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

const saveNote = async (note, cb) => {
  if ($('#save-close').is(':disabled')) {
    return;
  }

  if (typeof note !== 'string') {
    note = DOMPurify.sanitize(quill.root.innerHTML);
  }

  const accessLevel = $('#note-access-level input:checked').val();
  const bookIds = recentBooks?.getBookIdsForAddNote() || null;
  const deselectedBookIds = recentBooks?.getDeselectedBookIds() || null;
  const newBookTitle = recentBooks?.getNewBookTitle() || null;
  const noteId = document.getElementById('notes').dataset.noteId || null;

  displaySpinner();

  try {
    if (newBookTitle) {
      const response = await fetch(booksUrl, helper.setRequestParams('POST', { title: newBookTitle }));
      const result = await response.json();

      if (!result.error) {
        bookIds.push(result.data.book_id);
        recentBooks.enableNewBook(result.data.book_id);
      } else {
        recentBooks.enableNewBookActions();
        throw new Error('Error creating collection');
      }
    }

    const saveNoteRequestPayload = {
      url: pageUrl,
      title: pageTitle,
      note: note,
      book_ids: bookIds,
      access: accessLevel,
    };

    const tableValues = dataTable.getTableData();

    if (tableValues) {
      saveNoteRequestPayload['extracted_attrs'] = tableValues;
    }

    const reqPayload = [saveNoteRequestPayload];  // must be array
    const response = await fetch(noteUrl, helper.setRequestParams('POST', reqPayload));
    const result = await response.json();
    const { error, errmsg, errcode, status } = result;

    if (error) {
      if (isAuthErrorStatus(errcode)) {
        handleAuthError();
      } else if (
        status === helper.HttpCodes.badRequest &&
        errmsg === helper.errmsgs.urlTooLong
      ) {
        throw new Error(`Unable to save note: URL over ${helper.MAX_URL_LEN} chars.`);
      }
    } else {
      updateInlineUI(result.data);
      document.getElementById('notes').dataset.noteId = result.data;
      noteLog = note;
    }

    if (deselectedBookIds?.length !== 0 && noteId) {
      const reqPayload = {
        book_ids: deselectedBookIds,
        url_item_item_id: noteId,
      };
      const response = await fetch(removeNoteUrl, helper.setRequestParams('POST', reqPayload));
      if (response && !response.ok) {
        recentBooks?.reCheckBooks(deselectedBookIds);
        throw new Error(`Unable to remove note from collection (status ${response.status})`);
      }
    }

    updateSidebarUI({ note, bookIds, accessLevel, noteId: result.data || noteId });
    displayCheckmark();
  } catch (e) {
    displayAlert();
    if (e.message) {
      setMsg(e.message, 'errmsg');
    } else {
      setMsg('An error occured', 'errmsg');
    }
  }

  if (typeof cb === 'function') { cb(); }
};
utilsRefs.saveNote = saveNote;

const saveNoteDebounced = debounce((ifChanged = false) => {
  const note = DOMPurify.sanitize(quill.root.innerHTML);

  if (ifChanged) {
    if (note !== noteLog) {
      saveNote(note);
    }
  } else {
    saveNote(note);
  }
}, 800);
utilsRefs.saveNoteDebounced = saveNoteDebounced;

function removeFragment(encodedUrl) {
  const url = new URL(decodeURIComponent(encodedUrl));
  return url.origin + url.pathname + url.search;
}

function updateInlineUI(noteId) {
  chrome.storage.local.get(['featuresInline'], (result) => {
    if (result.featuresInline === true) {
      chrome.tabs.query({ url: removeFragment(pageUrl) }, (tabs) => {
        tabs.forEach((tab) => {
          chrome.tabs.sendMessage(tab.id, {
            action: 'setNoteIsPresent',
            noteId,
          });
        });
      });
    }
  });
}

function updateSidebarUI(data) {
  chrome.storage.local.get(['featuresInline'], (result) => {
    if (result.featuresInline === true) {
      chrome.tabs.query({ url: removeFragment(pageUrl) }, (tabs) => {
        tabs.forEach((tab) => {
          chrome.tabs.sendMessage(tab.id, { action: 'updateNote', data });
        });
      });
    }
  });
}

const fadingMsg = (msg) => {
  document.getElementById('notes-msg').textContent = msg;
  setTimeout(() => {
    document.getElementById('notes-msg').textContent = '';
  }, 2000);
};

const setMsg = (msg, classNames, isHTML = false) => {
  const span = document.createElement('span');
  if (isHTML) {
    span.innerHTML = DOMPurify.sanitize(msg);
  } else {
    span.textContent = msg;
  }
  if (classNames) {
    span.setAttribute('class', classNames);
  }
  const msgWrap = document.getElementById('notes-msg');
  msgWrap.textContent = '';
  msgWrap.appendChild(span);
};

function displaySpinner() {
  hideAlert();
  hideCheckmark();
  $('#popup-wrap .saving-spinner').removeClass('hide');
}

function hideSpinner() {
  $('#popup-wrap .saving-spinner').addClass('hide');
}

function displayCheckmark() {
  // Clear any error messages on success
  setMsg('');

  hideSpinner();
  $('#popup-wrap .saving-info--checkmark').removeClass('hide');
}

function hideCheckmark() {
  $('#popup-wrap .saving-info--checkmark').addClass('hide');
}

function displayAlert() {
  hideSpinner();
  $('#popup-wrap .saving-info--error').removeClass('hide');
}

function hideAlert() {
  $('#popup-wrap .saving-info--error').addClass('hide');
}

function hideLoadingUI() {
  $('#popup-wrap .loading-spinner').addClass('hide');
  $('#popup-wrap').removeClass('popup-placeholder');
  if ($('#notes').css('display') === 'block') {
    quill.focus();
  } else if ($('#savetabs').css('display') === 'block') {
    $('#savetabs-title').select();
  }
}

function showAuthError() {
  disableNav = true;
  $('#popup-wrap .loading-spinner').addClass('hide');
  $('#auth-error').removeClass('hide');
}

function showErrorMessage(msg) {
  // Check if the user is authencticated and set history setting
  fetch(`${apiBase}/settings/`, helper.setRequestParams())
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      if (result.error) {
        helper.setIconWarning();

        const { error, errmsg, errcode } = result;
        if (isAuthErrorStatus(errcode)) {
          handleAuthError();
        } else {
          showNoteSetupErrorMsg(errcode, errmsg);
        }
      } else {
        $('#popup-wrap .loading-spinner').addClass('hide');
        $('#popup-wrap').removeClass('popup-placeholder');
        $('#notes').addClass('note-placeholder');
        $('#note-error').text(msg).removeClass('hide');

        if (typeof result.settings?.history !== 'undefined') {
          local_save_log_account(result.settings.history);
        } else {
          enableSettings = false;
        }
      }
    })
    .catch(() => {
      showAuthError();
    });
}

const saveClose = () => {
  const note = DOMPurify.sanitize(quill.root.innerHTML);
  saveNote(note, window.close);
};

const openlogin = () => {
  chrome.tabs.create({ url: loginPageUrl });
};

const runNoteSetup = (currentTabId) => {
  let url = `${noteUrl}?url=${pageUrl}`;
  if (isAddressableUrl) {
    url = `${url}&addressable=1`; // Set param to be parsed as true
  }
  setMsg('Loading notes... Please wait...');
  fetch(url, helper.setRequestParams())
    .then(function (response) {
      return response.json();
    })
    .then(async function (result) {
      if (result.error) {
        helper.setIconWarning();
        const { error, errmsg, errcode } = result;
        if (isAuthErrorStatus(errcode)) {
          handleAuthError();
        } else {
          showNoteSetupErrorMsg(errcode, errmsg);
        }
      } else {
        $('#save-close').prop('disabled', false);
        helper.unsetIconWarning();
        if (typeof result.settings?.history !== 'undefined') {
          local_save_log_account(result.settings.history);
        } else {
          enableSettings = false;
        }

        let dataExtractionResult = null;
        if (!result.data.attrs) {
          const urlDecoded = decodeURIComponent(pageUrl);
          const pageUrlParsed = new URL(urlDecoded);
          if (helper.isDataExtractable(urlDecoded)) {
            dataExtractionResult = await runDataExtraction(urlDecoded, currentTabId);
          } else if (pageUrlParsed.hostname === 'www.linkedin.com' && pageUrlParsed.pathname.indexOf("/in/") === 0) {
            const linkedInResponse = await fetch(`${extractUrl}?url=${pageUrl}`, helper.setRequestParams('GET'));
            const linkedInResult = await linkedInResponse.json();
            if (!linkedInResult.error) {
              dataExtractionResult = {
                data: linkedInResult.data,
              };
            }
          }
        } else {
          dataExtractionResult = {
            data: result.data.attrs,
          };
        }

        if (result.settings?.access_levels) {
          const accessLevels = utils.createAccessLevels(result.settings.access_levels, saveNote, quill);
          document.getElementById('note-access-level').appendChild(accessLevels);
        } else {
          const accessLevels = utils.createAccessLevels([["private", "Private"]], saveNote, quill);
          document.getElementById('note-access-level').appendChild(accessLevels);
        }
        try {
          if (result.data.access) {
            document.querySelector(`#note-access-level input[value=${result.data.access}]`).checked = true;
          } else if (result.settings?.access_default) {
            document.querySelector(`#note-access-level input[value=${result.settings.access_default}]`).checked = true;
          } else {
            document.querySelector('#note-access-level input[value=private]').checked = true;
          }
        } catch {
          document.querySelector('#note-access-level input[value=private]').checked = true;
        }

        if (result.data.item_id) {
          document.getElementById('notes').dataset.noteId = result.data.item_id;
        }

        setMsg('');
        quill.enable();
        if (result.data && result.data.note > '') {
          quill.clipboard.dangerouslyPasteHTML(
            null,
            DOMPurify.sanitize(result.data.note || ''),
            'api'
          );
          noteLog = result.data.note;
        }
        quill.focus();
        quill.setSelection(quill.getLength());

        const suggestedTagsWrap = document.getElementById('suggested-tags-wrap');
        if (
          result.data.suggested_tags &&
          result.data.suggested_tags.length > 0
        ) {
          suggestedTagsWrap.appendChild(utils.createSuggestedTags(result.data.suggested_tags, quill));
        } else {
          suggestedTagsWrap.appendChild(utils.createSuggestedTags(['toread', 'work'], quill));
        }
        suggestedTagsWrap.classList.remove('hide');

        if (result.data.recent_books && result.data.recent_books.length > 0) {
          if (recentBooks) {
            recentBooks.renderRecentBooks(result.data.recent_books);
          } else {
            recentBooks = utils.createRecentBooks(result.data.recent_books, saveNote, saveNoteDebounced, quill);
            document.getElementById('recent-books').appendChild(recentBooks.element);
          }
        }

        if (dataExtractionResult) {
          renderExtractedData(dataExtractionResult);
        } else {
          renderExtractedData({ data: null });
        }

        hideLoadingUI();

        chrome.storage.local.get(['featuresEnhanceWebsites'], (res) => {
          if (res.featuresEnhanceWebsites === true) {
            if (
              result.data.note_taken_from &&
              result.data.note_taken_from.length > ''
            ) {
              // Tell user that note comes from another URL
              setMsg(
                `You made this note on a <a href="${result.data.note_taken_from}" target=”_blank” rel="noreferrer">different page</a> with the same content`,
                'addressable-msg',
                true,
              );
            }
          } else if (typeof res.featuresEnhanceWebsites === 'undefined') {
            setMsg('Enable "Enhance Websites" in settings to extract metadata');
          }
        });
      }
    })
    .catch((e) => {
      showAuthError();
    });
};

const handleResponse = (resp, errorMsg) => {
  if (!resp) {
    return;
  }

  if (resp.ok) {
    return resp.json();
  } else {
    throw new Error(errorMsg);
  }
};

const checkContentAddressableMapping = (url) => (
  contentAddressableMapping && contentAddressableMapping[url]
);

const updateContentAddressableMappingAndSetupNote = (
  currentUrl,
  currentPageContent,
  currentTabId
) => {
  const reqPayload = {
    url: currentUrl,
    page_content: currentPageContent,
  };
  fetch(
    contentAddressableApiUrl,
    helper.setRequestParams('POST', reqPayload),
  )
    .then((response) => {
      return handleResponse(
        response,
        'Error setting up notes. Please try closing the popup, refreshing the page, and re-opening the popup.'
      );
    })
    .then((result) => {
      if (result && result.data) {
        contentAddressableMapping = result.data;
      }
      isAddressableUrl = checkContentAddressableMapping(currentUrl);
      return runNoteSetup(currentTabId);
    })
    .catch((error) => {
      showErrorMessage(error.message);
    });
};

const setupContentAddressableNote = (currentTab) => {
  const currentUrl = currentTab.url;
  const currentTabId = currentTab.id;

  fetch(contentAddressableApiUrl, helper.setRequestParams())
    .then((response) => {
      return handleResponse(
        response,
        'Error setting up notes. Please try closing the popup, refreshing the page, and re-opening the popup.'
      );
    })
    .then((result) => {
      if (result.data) {
        contentAddressableMapping = result.data;
      }

      if (checkContentAddressableMapping(currentUrl)) {
        isAddressableUrl = true;
        return runNoteSetup(currentTabId);
      }

      // Insert current URL and content into content addressable table on server
      chrome.permissions.contains(
        {
          origins: ['*://*/*'],
        },
        (hasPermission) => {
          if (hasPermission) {
            chrome.tabs.sendMessage(
              currentTabId,
              { addressable: currentUrl },
              (msg) => {
                let pageContent = null;
                if (msg !== undefined) {
                  pageContent = msg.result;
                }
                return updateContentAddressableMappingAndSetupNote(
                  currentUrl,
                  pageContent,
                  currentTabId
                );
              }
            );
          } else {
            return runNoteSetup(currentTabId);
          }
        }
      );
    })
    .catch((error) => {
      showErrorMessage(error.message);
    });
};

// Data extraction -- start
const runDataExtraction = async (sourceUrl, tabId) => {
  return new Promise((resolve) => {
    chrome.permissions.contains({ origins: ['*://*/*'] }, (enabled) => {
      if (chrome.runtime.lastError) { }

      if (enabled) {
        chrome.tabs.executeScript(tabId, { file: 'extractable.js' }, (result) => {
          if (chrome.runtime.lastError) { }

          chrome.tabs.sendMessage(tabId, { extractable: sourceUrl }, (result) => {
            if (chrome.runtime.lastError) { }

            resolve(result?.result);
          });
        });
      } else {
        resolve(null);
      }
    });
  });
};

const renderExtractedData = (extractableResult = {}) => {
  const { data, errorMsg } = extractableResult;

  if (errorMsg) {
    // Currently on page with extractable data but none were found
    setMsg(errorMsg);
  } else {
    dataTable.renderExtractedData(data);
  }
};
// Data extraction -- end

document.addEventListener('DOMContentLoaded', function () {
  if (isDevMode) {
    document.getElementById('notes-debug').textContent = 'local';
  }

  // Setup data extraction table
  const $dataTableWrap = document.getElementById('extracted-data-table-wrap');
  $dataTableWrap.append(...dataTable.elements);

  document.getElementById('save-close').addEventListener('click', () => {
    const note = DOMPurify.sanitize(quill.root.innerHTML);
    saveNote(note, window.close);
  });

  $('body').on('keydown', (e) => {
    // Prevents ESC key from closing popup when pressed inside search/create box
    if (e.key === 'Escape') {
      e.preventDefault();
      if (e.target.hasAttribute('data-histre-book-input')) {
        return;
      } else {
        window.close();
      }
    }
  });

  chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
    if (
      !tabs ||
      typeof tabs[0] === 'undefined' ||
      !tabs[0] ||
      !tabs[0].url ||
      tabs[0].url.indexOf('http') !== 0
    ) {
      showErrorMessage('No web page found on this tab');

      // Break note setup
      return;
    }

    const currentUrl = tabs[0].url;
    currentTabId = tabs[0].id;

    pageUrl = encodeURIComponent(tabs[0].url);
    pageTitle = tabs[0].title;

    if (currentUrl.startsWith('https://www.youtube.com/watch?')) {
      chrome.permissions.contains({ origins: ['https://www.youtube.com/'] }, (enabled) => {
        const $editorWrap = document.querySelector('#editor-container');
        if ($editorWrap.querySelector('.yt-timestamp')) {
          return;
        }

        if (enabled) {
          chrome.tabs.executeScript(currentTabId, { file: 'youtube-player-control.js' }, () => {
            const ytTimestamp = utils.createYTTimestamp(currentTabId, currentUrl);
            $editorWrap.appendChild(ytTimestamp.element);
            $editorWrap.querySelector('#editor').addEventListener('click', ytTimestamp.jumpToYoutubeTimpestamp);
          });
        } else {
          const $ytEnable = document.createElement('div');
          $ytEnable.setAttribute('class', 'yt-timestamp');
          $ytEnable.innerHTML = '<i class="fe fe-youtube"></i> Save current time';
          $ytEnable.addEventListener('click', function () {
            chrome.permissions.request({ origins: ['https://www.youtube.com/'] });
          });
          $editorWrap.appendChild($ytEnable);
          $editorWrap.querySelector('#editor').addEventListener('click', () => {
            chrome.permissions.request({ origins: ['https://www.youtube.com/']});
          });
        }
      });
    }

    if (helper.isContentAddressable(tabs[0].url)) {
      setupContentAddressableNote(tabs[0]);
    } else {
      runNoteSetup(currentTabId);
    }
  });

  const bindings = {
    // "Cmd/Ctrl Enter" to save and close
    custom: {
      key: 13,
      shortKey: true,
      handler: function (range, context) {
        saveClose();
      },
    },
  };

  const mention = utils.getMentionConfig();

  quill = new Quill('#editor', {
    theme: 'snow',
    modules: {
      toolbar: false,
      keyboard: {
        bindings,
      },
      mention,
      magicUrl: true,
    },
  });
  utilsRefs.quill = quill;
  quill.disable();
  quill.on('text-change', function (delta, oldDelta, source) {
    if (source == 'api') {
      // pass
    } else if (source == 'user') {
      saveNoteDebounced(true);
    }
  });

  $('.nav-item').on('click', function (event) {
    event.preventDefault();

    if (disableNav) {
      return;
    }

    const el = $(this);
    const target = `#${el.data('contents-id')}`;

    if (target === '#settings') {
      if (!enableSettings) {
        return;
      }

      if (chrome.runtime.openOptionsPage) {
        chrome.runtime.openOptionsPage();
      } else {
        window.open(chrome.runtime.getURL('options.html'));
      }

      return;
    }

    $('.nav-item .nav-link').removeClass('active');
    el.find('.nav-link').addClass('active');
    $('.tab-contents').hide();
    $(target).show();

    if (target === '#savetabs') {
      $('#savetabs-title').val(`Tabs at ${helper.friendlyTime()}`).select();
    } else if (target === '#notes') {
      quill.focus();
    }
  });

  document
    .getElementById('do-save-tabs-thiswin')
    .addEventListener('click', (e) => {
      saveOpenTabs(true);
    });
  document
    .getElementById('do-save-tabs-allwin')
    .addEventListener('click', (e) => {
      saveOpenTabs(false);
    });

  $('#savetabs-title').keydown((e) => {
    if (e.ctrlKey && e.key === 'Enter') {
      saveOpenTabs(true);
    }
  });

  document.getElementById('editor').addEventListener('click', (e) => {
    quill.focus();
  });
});

function local_save_log_account(value) {
  chrome.storage.local.set({ userLogHistory: value });
}

const handleUserTagUpdate = (storedTags, storedTagsLastModified, updateCacheFunc) => {
  fetchUserTags(
    {
      url: tagUrl,
      options: helper.setRequestParams(),
    },
    {
      storedTags,
      storedTagsLastModified,
      updateCacheFunc,
    },
  )
    .then((result) => {
      utilsRefs.tagList = convertTagsHashToList(result);
    })
    .catch((error) => {
      setMsg('Error fetching tags');
    });
};

const cacheUpdatedTags = (updatedTags, latestModified) => {
  chrome.storage.local.set({
    [USER_TAGS_CACHE_KEY]: updatedTags,
    [USER_LAST_TAG_MODIFIED_KEY]: latestModified,
  });
};

function updateTagList() {
  chrome.storage.local.get([USER_TAGS_CACHE_KEY, USER_LAST_TAG_MODIFIED_KEY], (result) => {
    const storedTags = result[USER_TAGS_CACHE_KEY] || {};
    const storedTagsLastModified = result[USER_LAST_TAG_MODIFIED_KEY] || null;
    handleUserTagUpdate(storedTags, storedTagsLastModified, cacheUpdatedTags);
  });
}

updateTagList();

function onError(error) {
  helper.llog('onError', error);
}

function saveOpenTabs(current) {
  let tabsInfo = {};
  const queryOptions = { url: ['http://*/*', 'https://*/*'] };
  const bookTitle = $('#savetabs-title').val().trim();

  if (current) {
    queryOptions.currentWindow = true;
  }
  chrome.tabs.query(queryOptions, (tabs, onError) => {
    tabs.forEach((tab) => {
      const { windowId } = tab;
      if (typeof tabsInfo[windowId] !== 'object') {
        tabsInfo[windowId] = [];
      }
      tabsInfo[windowId].push({ url: tab.url, title: tab.title });
    });
    helper.llog(tabsInfo);

    var data = {
      items: tabsInfo,
      friendly_time: helper.friendlyTime(),
      title: bookTitle,
    };

    fetch(tabSaveUrl, helper.setRequestParams('POST', data))
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        const { data: { url }, error, errcode } = result;
        if (error) {
          if (isAuthErrorStatus(errcode)) {
            helper.handleAuthErrorsOnce(() => {
              setMsg('Not authenticated', 'errmsg');
              return;
            });
          } else {
            throw new Error('Error saving tabs');
          }
        } else {
          chrome.storage.local.get(['featuresInline'], (result) => {
            if (result.featuresInline === true) {
              tabs.forEach((tab) => {
                chrome.tabs.sendMessage(tab.id, { action: 'setNoteIsPresent' });
              });
            }
          });
          return url;
        }
      })
      .then((url) => {
        if (url) {
          const creating = chrome.tabs.create({ url: url }, (entry) => {
            if (chrome.runtime.lastError) {
              setMsg(
                `Saved tabs, but unable to open that page: ${url}`,
                'errmsg'
              );
            } else {
              window.close();
            }
          });
        }
      })
      .catch((error) => {
        setMsg(
          'Error saving tabs. Please email support@histre.com for help.',
          'errmsg'
        );
      });
  });
}
