(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('inline', classNames);
  const host = window.histreDOMHelper.getHost();

  const utils = new window.histreUtils('inline');
  const votes = utils.createVoteUI();

  // For drag functionality
  let mouseX = 0;
  let mouseY = 0;
  let leftOffset = '';
  let topOffset = '';

  function nukeSelf() {
    window.histreLoadedInline = false;
    clearOld();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
  }

  // clear elements from previous invocations
  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('inline.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('inline.css')}"]`).remove();
    }

    if (cb) { cb(); }
  };

  function openHistre() {
    window.open(host);
  };

  function openSettings() {
    //? Opening settings page doesn't work with inline script
    try {
      chrome.runtime.sendMessage({ action: 'openOptionsPage', dest: 'inline' }, function (response) {
        if (chrome.runtime.lastError) {
          console.log(JSON.stringify(chrome.runtime.lastError));
        }
        if (response && response.usermsg) {
          // TODO: Replace below with logic to show error message to user
          console.log(response.usermsg);
        }
      });
    } catch (e) {
      // connection to background scripts lost, perhaps due to extn reload
      // clear self etc so the user reloads the page
      nukeSelf();
    }
  }

  function openInfo() {
    window.open('https://histre.com/features/votes/');
  };

  function openSidebar() {
    const sidebarWrap = document.getElementById('histre-sidebar-wrap');
    if (sidebarWrap) {
      $wrap.classList.add(prefix('d-none'));
      sidebarWrap.style.display = 'flex';
      chrome.runtime.sendMessage({ action: 'focusEditor', dest: 'sidebar' });
      chrome.storage.local.set({ sidebarIsOpen: true });
      chrome.runtime.sendMessage({ action: 'sendEvent', dest: 'sidebar', data: { event: 'sidebar-max' } });
    }
  }

  function getUserInfo(cb) {
    chrome.storage.local.get(['userUsername', 'userExternalId'], (items) => {
      if (items.userUsername) {
        return cb({
          username: items.userUsername,
          externalId: items.userExternalId,
        });
      } else {
        chrome.runtime.sendMessage({ action: 'setUserInfo', dest: 'inline' }, {}, (res) => {
          if (res.isSet) {
            getUserInfo(cb);
          } else {
            //TODO Implement some sort of error message or figure out how to disable menu on user signout
            return;
          }
        });
      }
    });
  }

  function setNoteIsPresent(noteId) {
    if ($logoImg.getAttribute('src') !== chrome.runtime.getURL('assets/inline/logo-filled.svg')) {
      if (!noteId) {
        chrome.runtime.sendMessage({ action: 'getNoteIdAndSetNoteIsPresent', dest: 'inline' });
        return;
      }

      $logoImg.setAttribute('src', chrome.runtime.getURL('assets/inline/logo-filled.svg'));
      $logo.setAttribute('title', 'View note');
      $logo.removeEventListener('click', openHistre);
      getUserInfo(({ username }) => {
        $logo.addEventListener('click', () => {
          window.open(`${host}/notes/${username}/${noteId}`);
        });
      });
    }
  }

  function onMessageHandler(message) {
    if (message.action === 'showVote' && message.data) {
      votes.updateVote(message.data.vote);

      if (message.data.note_modified) {
        setNoteIsPresent(message.data.item_id);
      }
      return;
    }

    if (message.action === 'updateVoteInline') {
      votes.updateVote(message.vote);
      return;
    }

    if (message.action === 'loading') {
      return;
    }

    if (message.action === 'disableInline') {
      nukeSelf();
      return;
    }

    if (message.action === 'positionInline') {
      chrome.storage.local.get(['inlinePosTop', 'inlinePosLeft'], (result) => {
        // If it's undefined it will not be set
        $wrap.style.top = result.inlinePosTop;
        $wrap.style.left = result.inlinePosLeft;
      });
      return;
    }

    if (message.action === 'setNoteIsPresent') {
      setNoteIsPresent(message.noteId || null);
      return;
    }
  };

  function startInlineDrag(e) {
    e.preventDefault();

    mouseX = e.clientX;
    mouseY = e.clientY;

    document.addEventListener('mouseup', stopInlineDrag);
    document.addEventListener('mousemove', inlineDrag);
  }

  function inlineDrag(e) {
    e.preventDefault();

    const dragX = mouseX - e.clientX;
    const dragY = mouseY - e.clientY;

    // Refresh mouse coordinates
    mouseX = e.clientX;
    mouseY = e.clientY;

    // Prevent moving inline offscreen
    if (($wrap.offsetLeft - dragX) < ($wrap.clientWidth / 2)) {
      leftOffset = `${$wrap.clientWidth / 2}px`;
    } else if (($wrap.offsetLeft - dragX) > (window.innerWidth - ($wrap.clientWidth / 2))) {
      leftOffset = `calc(100vw - ${$wrap.clientWidth / 2}px`;
    } else {
      leftOffset = `${(($wrap.offsetLeft - dragX) / window.innerWidth * 100)}vw`;
    }

    if (($wrap.offsetTop - dragY) < 0) {
      topOffset = '0';
    } else if (($wrap.offsetTop - dragY) > (window.innerHeight - $wrap.clientHeight)) {
      topOffset = `calc(100vh - ${$wrap.clientHeight}px)`;
    } else {
      topOffset = `${($wrap.offsetTop - dragY) / window.innerHeight * 100}vh`;
    }

    $wrap.style.left = leftOffset;
    $wrap.style.top = topOffset;
  }

  function stopInlineDrag() {
    document.removeEventListener('mouseup', stopInlineDrag);
    document.removeEventListener('mousemove', inlineDrag);

    chrome.storage.local.set({ inlinePosTop: topOffset, inlinePosLeft: leftOffset });
  }

  const $css = $e(
    'link',
    {
      rel: 'stylesheet',
      type: 'text/css',
      href: chrome.runtime.getURL('inline.css'),
    }
  );

  const $drag = $e('div', { id: prefix('drag'), class: prefix('icon icon-drag') }, { mousedown: startInlineDrag });

  const $logoImg = $e('img', { id: prefix('logo-img'), src: chrome.runtime.getURL('assets/inline/logo.svg'), alt: 'Histre logo' });
  const $logoTxt = $e('div', { class: prefix('txt') }, noEvents, [$t('histre')]);
  const $logo = $e('div', { id: prefix('logo'), class: prefix('logo') }, { click: openHistre }, [$logoImg, $logoTxt]);

  const $settings = $e('div', { id: prefix('settings'), class: prefix('icon icon-settings') }, { click: openSettings });
  const $info = $e('div', { id: prefix('info'), class: prefix('icon icon-info') }, { click: openInfo });

  const $maximize = $e('div', { id: prefix('down'), class: prefix('icon icon-maximize') }, { click: openSidebar });

  const $wrap = $e(
    'div',
    {
      id: prefix('wrap'),
      style: 'display: none;', // prevent some strange interactions until styles are loaded
    },
    noEvents,
    [$drag, $logo, ...votes.elements, $info, $settings, $maximize],
  );

  clearOld(() => {
    chrome.storage.local.get(['inlinePosTop', 'inlinePosLeft', 'sidebarIsOpen'], (result) => {
      if (typeof result.inlinePosTop !== 'undefined' && typeof result.inlinePosLeft !== 'undefined') {
        $wrap.style.top = result.inlinePosTop;
        $wrap.style.left = result.inlinePosLeft;
      }

      if (result.sidebarIsOpen) {
        $wrap.classList.add(prefix('d-none'));
      }

      (document.head || document.documentElement).appendChild($css);
      document.body.appendChild($wrap);
      chrome.runtime.onMessage.addListener(onMessageHandler);
    });
  });

  return true;
})();
