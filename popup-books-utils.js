// Functions for rendering/handling books data on extn

const setupRecentBooks = () => {
  const recentBooksEl = document.getElementById('recent-books-body');
  recentBooksEl.textContent = '';
  toggleExpandBookListcontrol('hide');
  if (!recentBooks) {
    return;
  }

  const orgAccessLabel = document.querySelector('label[for=btn-access-org]')?.title;

  recentBooks.forEach((book, bookIndex) => {
    const bookItemWrap = document.createElement('div');

    const bookCheckWrap = document.createElement('div');
    bookCheckWrap.setAttribute('class', 'book-check-wrap');
    bookCheckWrap.setAttribute('role', 'checkbox');
    bookCheckWrap.setAttribute('aria-checked', 'false');
    bookCheckWrap.setAttribute('tabindex', '0');

    const bookCheck = document.createElement('i');
    const bookCheckId = `recent-book-${book.book_id}`;
    let bookCheckClasses = 'book-check fe fe-circle';

    if (book.current_url_item_id) {
      // User has previously added this URL to this notebook
      bookCheckClasses = 'book-check fe fe-check-circle book-check-checked';
      bookCheck.setAttribute('data-url-item-id', book.current_url_item_id);
      bookCheckWrap.setAttribute('aria-checked', 'true');
    }

    bookCheck.setAttribute('class', bookCheckClasses);
    bookCheck.setAttribute('id', bookCheckId);
    bookCheck.setAttribute('data-book-id', book.book_id);
    bookCheckWrap.appendChild(bookCheck);

    const bookIcon = document.createElement('i');
    const { access } = book;
    let bookIconClass, info;
    if (access === 'public') {
      bookIconClass = 'fe-globe';
      info = 'Visible to public';
    } else if (book.team_member_usernames.length > 0) {
      bookIconClass = 'fe-share-2';
      info = `Shared with ${book.team_member_usernames.length} people`;
    } else if (access === 'org') {
      const orgText = (orgAccessLabel) ? orgAccessLabel.toLowerCase() : 'your org';
      bookIconClass = 'fe-users';
      info = `Visible to ${orgText}`;
    } else {
      bookIconClass = 'fe-lock';
    }
    bookIcon.setAttribute('class', `book-icon fe fe-circle ${bookIconClass}`);
    bookCheckWrap.appendChild(bookIcon);

    const bookName = document.createElement('div');
    bookName.setAttribute('class', 'book-name');
    bookName.textContent = book.title;
    bookCheckWrap.appendChild(bookName);

    const bookShareableLink = document.createElement('div');
    bookShareableLink.setAttribute('class', 'book-action book-action--copy');
    bookShareableLink.setAttribute('role', 'button');
    bookShareableLink.setAttribute('tabindex', '0');
    bookShareableLink.setAttribute('aria-label', 'Copy collection link to clipboard');
    bookShareableLink.setAttribute('title', 'Copy collection link to clipboard');
    bookShareableLink.setAttribute('data-book-link', book.shareable_link);
    bookShareableLink.setAttribute('data-action', 'copy');

    bookShareableLink.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path d="M16 4h2a2 2 0 0 1 2 2v4M8 4H6a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2"></path><rect width="8" height="4" x="8" y="2" rx="1" ry="1"></rect><path d="M21 14H11m4-4l-4 4l4 4"></path></g></svg>';

    const bookShareableLinkCheckmark = document.createElement('i');
    bookShareableLinkCheckmark.setAttribute('class', 'fe fe-check-circle hide');
    bookShareableLinkCheckmark.setAttribute('aria-label', 'Link copied to clipboard');
    bookShareableLinkCheckmark.setAttribute('title', 'Link copied to clipboard');

    bookShareableLink.appendChild(bookShareableLinkCheckmark);
    bookCheckWrap.appendChild(bookShareableLink);

    const bookOpenLink = document.createElement('div');
    bookOpenLink.setAttribute('class', 'book-action');
    bookOpenLink.setAttribute('role', 'button');
    bookOpenLink.setAttribute('tabindex', '0');
    bookOpenLink.setAttribute('title', 'View collection details');
    bookOpenLink.setAttribute('aria-label', 'View collection details');
    bookOpenLink.setAttribute('data-book-id', book.book_id);
    bookOpenLink.setAttribute('data-action', 'view');

    const bookOpenLinkIcon = document.createElement('i');
    bookOpenLinkIcon.setAttribute('class', 'fe fe-link');

    bookOpenLink.appendChild(bookOpenLinkIcon);
    bookCheckWrap.appendChild(bookOpenLink);

    bookItemWrap.appendChild(bookCheckWrap);

    const bookInfo = document.createElement('div');
    bookInfo.setAttribute('class', 'book-info');
    bookInfo.textContent = info;
    bookItemWrap.appendChild(bookInfo);

    if (bookIndex >= maxBooksDisplayed) {
      bookItemWrap.setAttribute('class', 'more-book-items hide');
    }

    recentBooksEl.appendChild(bookItemWrap);
  });

  if (recentBooks.length > maxBooksDisplayed) {
    toggleExpandBookListcontrol('show');
  }

  showOrHideRecentBooks();
};

const countSelectedBooks = () => {
  const result = document.querySelectorAll(
    '#recent-books-body .book-check-checked'
  );
  return result.length;
};

const updateShowMoreText = () => {
  const selectedBooksCount = countSelectedBooks();
  const showMoreLink = document.getElementById('expand-book-list-toggle');
  showMoreLink.textContent = `Show more (${selectedBooksCount} selected)`;
};

const toggleExpandBookListcontrol = (controlAction) => {
  // Shows or hide the 'Show more' link
  const expandBookListToggleWrap = document.getElementById(
    'expand-book-list-toggle-area'
  );
  if (controlAction === 'show') {
    expandBookListToggleWrap.classList.remove('hide');
    updateShowMoreText();
  } else if (controlAction === 'hide') {
    expandBookListToggleWrap.classList.add('hide');
  }
};

const showMoreBookItems = () => {
  const moreBookItems = document.querySelectorAll('.more-book-items');
  Array.from(moreBookItems).forEach((bookItem) => {
    bookItem.classList.remove('hide');
  });
};

const showRecentBooks = () => {
  chrome.storage.local.set({ popupShowRecentBooks: true }, () => {
    showOrHideRecentBooks();
  });
};

const hideRecentBooks = () => {
  chrome.storage.local.set({ popupShowRecentBooks: false }, () => {
    showOrHideRecentBooks();
  });
};

const showOrHideRecentBooks = () => {
  chrome.storage.local.get(['popupShowRecentBooks'], (result) => {
    let show = result.popupShowRecentBooks;
    if (typeof show === 'undefined') {
      show = true; // default
    }
    if (show) {
      $('#recent-books').show();
      $('#recent-books-closed').hide();
    } else {
      $('#recent-books').hide();
      $('#recent-books-closed').show();
    }
  });
};

const toggleBookSearchBox = () => {
  const searchToggleButton = $('#recent-books-search-toggle');
  const searchArea = $('#recent-books-search-area');
  if (searchArea.is(':visible')) {
    searchToggleButton.attr('aria-pressed', 'false');
    searchArea.hide();
    quill.focus();
  } else {
    const createBookArea = $('#create-book-area');
    if (createBookArea.is(':visible')) {
      toggleBookCreateArea();
    }
    searchToggleButton.attr('aria-pressed', 'true');
    searchArea.show();
    updateSearchStatus();
    $('#recent-books-search-box').focus();
  }
};

const toggleBookCreateArea = () => {
  const createBookToggleButton = $('#create-book-toggle');
  const createBookArea = $('#create-book-area');
  if (createBookArea.is(':visible')) {
    createBookToggleButton.attr('aria-pressed', 'false');
    createBookArea.hide();
    $('#new-book-title').val('');
    quill.focus();
  } else {
    const searchArea = $('#recent-books-search-area');
    if (searchArea.is(':visible')) {
      toggleBookSearchBox();
    }
    createBookToggleButton.attr('aria-pressed', 'true');
    createBookArea.show();
    $('#new-book-title').focus();
  }
};

const getDeselectedBookIds = () => {
  // Returns array of de-selected notebook IDs
  // (previously linked notebooks that are now unchecked)
  let deselectedBookIds = [];
  const uncheckedBooks = document.getElementsByClassName(
    'book-check fe-circle'
  );
  Array.from(uncheckedBooks).forEach((book) => {
    if ('urlItemId' in book.dataset) {
      deselectedBookIds.push(book.dataset.bookId);
    }
  });
  return deselectedBookIds;
};

const getCurrentUrlItemId = () => {
  const linkedBookItem = document.querySelector('[data-url-item-id]');
  if (!linkedBookItem) {
    return;
  }
  return linkedBookItem.dataset.urlItemId;
};

const updateSearchStatus = (msg = null, classNames = null) => {
  const searchStatusArea = document.getElementById(
    'recent-books-search-status'
  );
  const currentSearchStatus = searchStatusArea.textContent.trim();
  if (!msg || !currentSearchStatus) {
    searchStatusArea.textContent = 'Type a collection title or keyword';
  } else if (msg) {
    searchStatusArea.textContent = msg;
  }

  if (classNames) {
    searchStatusArea.setAttribute('class', classNames);
  }
};
