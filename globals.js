// ENV : local | stg | prod
const ENV = 'local';

const isDevMode = ENV !== 'prod';

let host = 'https://histre.com';
if (ENV === 'local') {
  host = 'http://local.histre.com:8080';
} else if (ENV === 'stg') {
  host = 'https://cfgromit.histre.com';
}

const apiBase = `${host}/api/v1`;

const isFirefox = typeof browser === 'object';
if (isFirefox) {
  chrome = browser;
}
