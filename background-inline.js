(function () {
  const voteUrl = `${apiBase}/votes/`;
  const noteUrl = `${apiBase}/note/`;
  const tagUrl = `${apiBase}/tag/`;
  const booksUrl = `${apiBase}/collections/`;
  const eventUrl = `${apiBase}/event/`;
  const extractUrl = `${apiBase}/extract/`;
  const removeNoteUrl = `${booksUrl}remove_note/`;

  let isInlineEnabled = false;

  const authErrorMessage = 'Unable to authenticate with histre.com';

  let topInlineOffset = '';
  let leftInlieOffset = '';

  const domainBlacklist = [
    "mail.google.com",
    "calendar.google.com",
    "slack.com",
  ];

  // Convert some Chrome functions to async/await -- start
  function hasPermissions(permissions = {}) {
    return new Promise((resolve) => {
      chrome.permissions.contains(permissions, (enabled) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(enabled);
      });
    });
  }

  function executeScript(tabId, details) {
    return new Promise((resolve) => {
      if (typeof tabId === 'number') {
        chrome.tabs.executeScript(tabId, details, (result) => {
          if (chrome.runtime.lastError) {
            // TODO reject error
          }
          resolve(result);
        });
      } else {
        chrome.tabs.executeScript(tabId, (result) => {
          if (chrome.runtime.lastError) {
            // TODO reject error
          }
          resolve(result);
        });
      }
    });
  }

  function sendMessage(tabId, message) {
    return new Promise((resolve) => {
      chrome.tabs.sendMessage(tabId, message, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    });
  }

  function getLocalStorage(keys = null) {
    return new Promise((resolve) => {
      chrome.storage.local.get(keys, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    });
  }
  // Convert some Chrome functions to async/await -- end

  async function addInlineContentScript(tab) {
    if (!isInlineEnabled) { return; }

    // ignore chrome:// about:config etc
    if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) { return; }

    const tabUrl = new URL(tab.url);
    const tabHostname = tabUrl.hostname;

    for (domain of domainBlacklist) {
      if (tabHostname.includes(domain)) {
        return;
      }
    }

    const enabled = await hasPermissions({ origins: ['*://*/*'] });

    if (enabled) {
      async function loadInline() {
        const result = await executeScript(tab.id, { file: 'inline-loader.js' });
        if (!Array.isArray(result) || result.length <= 0) {
          //? Something went wrong
          throw result;
        }
        const [isInlineLoaded] = result;
        if (isInlineLoaded === false) {
          await executeScript(tab.id, { file: 'dom-helper.js' });
          await executeScript(tab.id, { file: 'utils.js' });
          await executeScript(tab.id, { file: 'inline.js' });
        }
      }

      function setupInline() {
        return new Promise((resolve, reject) => {
          helper.doGet(
            `${voteUrl}?url=${encodeURIComponent(tab.url)}`,
            {},
            (result) => {
              const { data, status } = result;
              if (status === helper.HttpCodes.success) {
                sendMessage(tab.id, { action: 'showVote', data: data });
                resolve(data);
              } else if (isAuthErrorStatus(status)) {
                handleAuthError();
                reject(new Error(authErrorMessage));
              } else {
                reject(result);
              }
            }
          );
        });
      }

      async function loadSidebar() {
        const result = await executeScript(tab.id, { file: 'sidebar-loader.js' });
        if (!Array.isArray(result) || result.length <= 0) {
          //? Something went wrong
          throw result;
        }
        const [isSidebarLoaded] = result;
        if (tab.url.startsWith('https://www.youtube.com/watch?')) {
          await executeScript(tab.id, { file: 'youtube-player-control.js' });
        }
        if (isSidebarLoaded === false) {
          await executeScript(tab.id, { file: 'quill.min.js' });
          await executeScript(tab.id, { file: 'purify.min.js' });
          await executeScript(tab.id, { file: 'quill.mention.min.js' });
          await executeScript(tab.id, { file: 'quill.magic-url.min.js' });
          await executeScript(tab.id, { file: 'dom-helper.js' });
          await executeScript(tab.id, { file: 'utils.js' });
          await executeScript(tab.id, { file: 'sidebar.js' });
        }
      }

      function setupSidebar(vote) {
        return new Promise(async (resolve, reject) => {
          await sendMessage(tab.id, { action: 'loading' });
          helper.doGet(`${noteUrl}?url=${encodeURIComponent(tab.url)}`, {}, async (result) => {
            if (!result.error) {
              result.tags = await getTagList();

              const tabUrlParsed = new URL(tab.url);
              if (!result.data.attrs) {
                if (helper.isDataExtractable(tab.url)) {
                  await executeScript(tab.id, { file: 'extractable.js' });
                  const dataExtraction = await sendMessage(tab.id, { extractable: tab.url });
                  result.dataExtraction = dataExtraction?.result;
                } else if (tabUrlParsed.hostname === 'www.linkedin.com' && tabUrlParsed.pathname.indexOf("/in/") === 0) {
                  const linkedInResponse = await fetch(`${extractUrl}?url=${encodeURIComponent(tab.url)}`, helper.setRequestParams('GET'));
                  const linkedInResult = await linkedInResponse.json();
                  if (!linkedInResult.error) {
                    result.dataExtraction = {
                      data: linkedInResult.data,
                    };
                  }
                }
              } else {
                result.dataExtraction = {
                  data: result.data.attrs,
                };
              }

              if (typeof vote !== 'undefined') {
                result.vote = vote;
              }

              await sendMessage(tab.id, { action: 'setupNote', result, tab });
              resolve();
            } else if (isAuthErrorStatus(result.status)) {
              handleAuthError();
              reject(new Error(authErrorMessage));
            } else {
              reject(result);
            }
          });
        });
      }

      try {
        await loadInline();
        await loadSidebar();

        try {
          const voteData = await setupInline();
          await setupSidebar(voteData.vote);
          helper.unsetIconWarning();
        } catch (e) {
          sendMessage(tab.id, { action: 'displayError', message: e.message ? e.message : 'An error occured' });
        }
      } catch (e) {
        helper.llog('Error loading inline/sidebar', tab, e);
      }
    }
  }

  function removeInlineContent(tab) {
    if (isInlineEnabled) { return; }

    // ignore chrome:// about:config etc
    if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) { return; }

    sendMessage(tab.id, { action: 'disableInline' });
    sendMessage(tab.id, { action: 'disableSidebar' });
  }

  function onMessageHandler(message, sender, sendResponse) {
    // Inline
    if (message.dest === 'inline') {
      if (message.action === 'saveInlineVote') {
        const data = {
          url: message.inline.url,
          title: message.inline.title,
          vote: message.inline.vote,
        };
        helper.doPost(voteUrl, data, (result) => {
          helper.llog('vote saved:' + result);

          if (result && result.data && result.data.redir_oauth) {
            chrome.tabs.create({ url: `${host}${result.data.redir_oauth}` });
          }
          if (result && result.errmsg === helper.errmsgs.urlTooLong) {
            result['usermsg'] = `Cannot save vote: URL over ${helper.MAX_URL_LEN} chars.`;
          }
          sendResponse(result);

          if (!result.error) {
            chrome.storage.local.get(['sidebarIsOpen'], (result) => {
              if (result.sidebarIsOpen) {
                chrome.tabs.sendMessage(sender.tab.id, { action: 'displayCheck' });
              }

              chrome.tabs.query({ url: removeFragment(sender.tab.url) }, (tabs) => {
                tabs.forEach((tab) => {
                  chrome.tabs.sendMessage(tab.id, {
                    action: 'updateVoteInline',
                    vote: message.inline.vote,
                  });
                  chrome.tabs.sendMessage(tab.id, {
                    action: 'updateVoteSidebar',
                    vote: message.inline.vote,
                  });
                });
              });
            });
          }
        });
        // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
        // See: Sending an asynchronous response using sendResponse
        // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
        return true;
      }

      if (message.action === 'openOptionsPage') {
        window.open(chrome.runtime.getURL('options.html') + '#votes');
        return;
      }

      if (message.action === 'setUserInfo') {
        helper.doGet(`${apiBase}/settings/`, {}, (response) => {
          if (response.data?.username && response.data?.external_id) {
            chrome.storage.local.set({
              userUsername: response.data.username,
              userExternalId: response.data.external_id,
            }, () => {
              sendResponse({ isSet: true });
            });
          } else {
            sendResponse({ isSet: false });
          }
        });
        return true;
      }

      if (message.action === 'setNoteIsPresent') {
        chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
          tabs.forEach((tab) => {
            chrome.tabs.sendMessage(tab.id, { action: 'setNoteIsPresent' });
          });
        });
        return;
      }

      if (message.action === 'getNoteIdAndSetNoteIsPresent') {
        helper.doGet(
          `${voteUrl}?url=${encodeURIComponent(sender.tab.url)}`,
          {},
          (result) => {
            const { data, status } = result;

            if (status === helper.HttpCodes.success) {
              chrome.tabs.sendMessage(sender.tab.id, { action: 'setNoteIsPresent', noteId: data.item_id });
            }
          }
        );
        return;
      }
    }

    // Sidebar
    if (message.dest === 'sidebar') {
      if (message.action === 'saveNote' && message.data) {
        // Ref: https://stackoverflow.com/questions/20435528/chrome-extension-sendresponse-does-not-work
        // onMessageHandler function can't be "natively" async because sendResponse won't work,
        // as it depends for return value to be boolean, and async function returns a Promise.
        (async () => {
          try {
            let newBookId = null;
            if (message.createBook) {
              const response = await fetch(booksUrl, helper.setRequestParams('POST', message.createBook));
              const result = await response.json();
              if (!result.error) {
                newBookId = result.data.book_id;
                if (Array.isArray(message.data[0].book_ids)) {
                  message.data[0].book_ids.push(newBookId);
                } else {
                  message.data[0].book_ids = [newBookId];
                }
                sendMessage(sender.tab.id, { action: 'enableNewBook', newBookId });
              } else if (isAuthErrorStatus(result.status)) {
                handleAuthError();
                throw new Error(authErrorMessage);
              } else {
                sendMessage(sender.tab.id, { action: 'enableNewBookActions' });
                throw new Error('Error creating collection');
              }
            }

            const response = await fetch(noteUrl, helper.setRequestParams('POST', message.data));
            const result = await response.json();

            if (!result.error) {
              // Send back note for autosave optimization - note log
              const note = message.data[0].note;
              sendMessage(sender.tab.id, { action: 'noteSaved', noteId: result.data, note });
            } else {
              if (
                result.status === helper.HttpCodes.badRequest &&
                result.errmsg === helper.errmsgs.urlTooLong
              ) {
                throw new Error(`Unable to save note: URL over ${helper.MAX_URL_LEN} chars.`);
              } else if (isAuthErrorStatus(result.status)) {
                handleAuthError();
                throw new Error(authErrorMessage);
              } else {
                throw new Error(`An error occured while saving note.`);
              }
            }

            if (message.removeNoteFromBooks) {
              const response = await fetch(removeNoteUrl, helper.setRequestParams('POST', message.removeNoteFromBooks));
              if (response && !response.ok) {
                sendMessage(sender.tab.id, { action: 'reCheckBooks', bookIds: message.removeNoteFromBooks.book_ids });
                throw new Error(`Unable to remove note from collection (status ${response.status})`);
              }
            }

            chrome.tabs.query({ url: removeFragment(sender.tab.url) }, (tabs) => {
              tabs.forEach((tab) => {
                sendMessage(tab.id, { action: 'setNoteIsPresent', noteId: result.data });

                if (tab.id !== sender.tab.id) {
                  addInlineContentScript(tab);
                }
              });
            });

            sendMessage(sender.tab.id, { action: 'displayCheck' });
          } catch (e) {
            sendMessage(sender.tab.id, { action: 'displayError', message: e.message ? e.message : 'An error occured' });
          }
        })();
        return;
      }

      if (message.action === 'focusEditor') {
        sendMessage(sender.tab.id, { action: 'focusEditor' });
        return;
      }

      if (message.action === 'closeRecentBooks') {
        chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
          tabs.forEach((tab) => {
            sendMessage(tab.id, { action: 'closeRecentBooks' });
          });
        });
        return;
      }

      if (message.action === 'showRecentBooks') {
        chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
          tabs.forEach((tab) => {
            sendMessage(tab.id, { action: 'showRecentBooks' });
          });
        });
        return;
      }

      if (message.action === 'reExtractData') {
        (async () => {
          const dataExtraction = await sendMessage(sender.tab.id, { extractable: sender.tab.url });
          sendMessage(sender.tab.id, { action: 'renderExtractedData', dataExtraction: dataExtraction?.result });
        })();
        return;
      }

      if (message.action === 'handleAuthError') {
        addInlineContentScript(sender.tab);
        return;
      }

      if (message.action === 'sendEvent') {
        helper.doPost(eventUrl, message.data, (result) => {
          if (result.status !== 200) {
            helper.llog('Error posting an sidebar event. Info:', result);
          }
        });
        return;
      }
    }
  }

  function onUpdatedHandler(tabId, changeInfo, tab) {
    if (!isInlineEnabled) { return; }

    // Exeption for docs.google
    const tabUrl = new URL(tab.url);
    if (tabUrl.hostname === 'docs.google.com') {
      if (changeInfo.status === 'complete') {
        addInlineContentScript(tab);
      }
      return;
    }

    // Only fire once, when tab is changing
    if (changeInfo.status === 'loading') {
      addInlineContentScript(tab);
    }
  }

  function refreshFromStorage() {
    chrome.storage.local.get(['featuresInline', 'inlinePosTop', 'inlinePosLeft'], (result) => {
      if (typeof result.featuresInline !== 'undefined') {
        if (isInlineEnabled !== result.featuresInline) {
          isInlineEnabled = result.featuresInline;

          if (isInlineEnabled) {
            enableInline();
          } else {
            disableInline();
          }
        }
      }

      if (typeof result.inlinePosTop !== 'undefined' && typeof result.inlinePosLeft !== 'undefined') {
        if (topInlineOffset !== result.inlinePosTop || leftInlieOffset !== result.inlinePosLeft) {
          topInlineOffset = result.inlinePosTop;
          leftInlieOffset = result.inlinePosLeft;

          updateInlinePosition();
        }
      }
    });
  }

  function enableInline() {
    chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
      tabs.forEach((tab, i) => {
        setTimeout(() => {
          addInlineContentScript(tab);
        }, i * delayUnitBetweenAPICalls);
      });
    });
  }

  function disableInline() {
    chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
      tabs.forEach((tab) => {
        removeInlineContent(tab);
      });
    });
  }

  function updateInlinePosition() {
    if (topInlineOffset === '' || leftInlieOffset === '') {
      return;
    }

    chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
      tabs.forEach((tab) => {
        sendMessage(tab.id, { action: 'positionInline' });
      });
    });
  }

  function isAuthErrorStatus(statusCode) {
    return (
      statusCode === helper.HttpCodes.unauthorized ||
      statusCode === helper.HttpCodes.forbidden
    );
  };

  function handleAuthError() {
    helper.setIconWarning();
    helper.handleAuthErrorsOnce();
  };

  async function getTagList() {
    const result = await getLocalStorage([USER_TAGS_CACHE_KEY, USER_LAST_TAG_MODIFIED_KEY]);
    const storedTags = result[USER_TAGS_CACHE_KEY] || {};
    const storedTagsLastModified = result[USER_LAST_TAG_MODIFIED_KEY] || null;

    return handleUserTagUpdate(storedTags, storedTagsLastModified, cacheUpdatedTags);
  }

  async function handleUserTagUpdate(storedTags, storedTagsLastModified, updateCacheFunc) {
    const result = await fetchUserTags(
      {
        url: tagUrl,
        options: helper.setRequestParams(),
      },
      {
        storedTags,
        storedTagsLastModified,
        updateCacheFunc,
      },
    )
    return convertTagsHashToList(result);
  };

  function cacheUpdatedTags(updatedTags, latestModified) {
    chrome.storage.local.set({
      [USER_TAGS_CACHE_KEY]: updatedTags,
      [USER_LAST_TAG_MODIFIED_KEY]: latestModified,
    });
  };

  function removeFragment(encodedUrl) {
    const url = new URL(decodeURIComponent(encodedUrl));
    return url.origin + url.pathname + url.search;
  }

  chrome.tabs.onUpdated.addListener(onUpdatedHandler);
  chrome.runtime.onMessage.addListener(onMessageHandler);
  chrome.storage.onChanged.addListener(() => { refreshFromStorage(); });
  chrome.runtime.onStartup.addListener(() => { refreshFromStorage(); });
  chrome.runtime.onInstalled.addListener(() => { refreshFromStorage(); });
})();
