# Histre Browser Extension

## About

[Histre](https://histre.com/) is an effortless knowledge base. It uses
this browser extension to log your notes, tags, bookmarks, and
browsing history.

## Compatibility

This extension works with Chrome, Chromium, and Firefox.
