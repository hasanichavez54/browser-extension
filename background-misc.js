(function () {

  function addAddressableContentScript(tab) {
    chrome.permissions.contains(
      {
        origins: ['*://*/*'],
      },
      function (result) {
        if (result) {
          chrome.tabs.executeScript(tab.id, {
            file: 'addressable.js',
          });
          if (chrome.runtime.lastError) {
          }
        } else {
          // we don't have the permission to do that
        }
      }
    );
  }

  function addTabsContentScript(tab) {
    chrome.permissions.contains(
      {
        origins: histre_origins,
      },
      function (result) {
        if (result) {
          chrome.tabs.executeScript(tab.id, {
            file: 'histre_tabs.js',
          });
          if (chrome.runtime.lastError) {
          }
        } else {
          // we don't have the permission to do that
        }
      }
    );
  }

  function closeInstallCardOnHomePage(tabId) {
    chrome.permissions.contains(
      {
        origins: histre_origins,
      },
      (enabled) => {
        if (enabled) {
          chrome.tabs.executeScript(tabId, {
            code: `document.getElementById('install-card-close-btn')?.click();`,
          });
        }
      }
    );
  }

  function tabUpdatedHandler(tabId, changes, tab) {
    if (!tab || !tab.url) {
      return;
    }

    const url = new URL(tab.url);

    if (url.protocol.indexOf('http') === 0 && changes.status === 'complete') {
      if (helper.isContentAddressable(tab.url)) {
        addAddressableContentScript(tab);
      }
    }

    if (url.hostname.includes('histre.com')) {
      if (url.pathname === '/') {
        closeInstallCardOnHomePage(tabId);
      } else if (url.pathname.includes('/collections/')) {
        // Include check for /public/collections/
        addTabsContentScript(tab);
      } else {
        // do nothing
      }
    }
  }

  chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
})();
