(function () {
  let featuresHighlights = null;
  const highlightUrl = `${apiBase}/highlight/`;

  // Convert some Chrome functions to async/await -- start
  function hasPermissions(permissions = {}) {
    return new Promise((resolve) => {
      chrome.permissions.contains(permissions, (enabled) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(enabled);
      });
    });
  }

  function executeScript(tabId, details) {
    return new Promise((resolve) => {
      if (typeof tabId === 'number') {
        chrome.tabs.executeScript(tabId, details, (result) => {
          if (chrome.runtime.lastError) {
            // TODO reject error
          }
          resolve(result);
        });
      } else {
        chrome.tabs.executeScript(tabId, (result) => {
          if (chrome.runtime.lastError) {
            // TODO reject error
          }
          resolve(result);
        });
      }
    });
  }

  function sendMessage(tabId, message) {
    return new Promise((resolve) => {
      chrome.tabs.sendMessage(tabId, message, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    });
  }

  function getLocalStorage(keys = null) {
    return new Promise((resolve) => {
      chrome.storage.local.get(keys, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    });
  }
  // Convert some Chrome functions to async/await -- end

  function enableHighlights() {
    chrome.tabs.onUpdated.addListener(tabUpdatedHandlerHighlights);
    chrome.tabs.query({ url: '*://*/*' }, function (tabs) {
      for (var i = 0; i < tabs.length; ++i) {
        setTimeout(() => {
          addHighlightsContentScript(tabs[i]);
        }, i * delayUnitBetweenAPICalls);
      }
    });
  }

  function disableHighlights() {
    chrome.tabs.onUpdated.removeListener(tabUpdatedHandlerHighlights);
    chrome.tabs.query({ url: '*://*/*' }, function (tabs) {
      const message = { action: 'hide_highlights' };
      for (var i = 0; i < tabs.length; ++i) {
        chrome.tabs.sendMessage(tabs[i].id, message);
      }
    });
  }

  function doPostHighlight(data, cb) {
    helper.doPost(highlightUrl, data, function (result) {
      helper.llog('highlight saved:' + result);
      cb(result);
    });
  }

  function doPatchHighlight(data, cb) {
    helper.doPatch(highlightUrl, data, function (result) {
      helper.llog('highlight updated:' + result);
      cb(result);
    });
  }

  function doDeleteHighlight(data, cb) {
    helper.doDelete(highlightUrl, data, function (result) {
      helper.llog('highlight deleted:' + result);
      cb(result);
    });
  }

  function histreContextMenuOnClickHandler(info, tab) {
    chrome.tabs.executeScript(
      tab.id,
      {
        file: 'range.js',
      },
      function (selection) {
        var data = {
          extra: {
            selection: selection,
          },
          url: tab.url,
          title: tab.title,
          text: info.selectionText,
        };
        doPostHighlight(data, function () {});
      }
    );
    if (chrome.runtime.lastError) {
    }
  }

  function addHistreContextMenu() {
    chrome.contextMenus.create({
      title: 'Save highlight to histre',
      contexts: ['selection'],
      onclick: histreContextMenuOnClickHandler,
    });
  }

  function onInstalledHandlerHighlights(object) {
    addHistreContextMenu();
    refresh_from_storage_highlight();
  }

  function onStartupHandlerHighlights(object) {
    addHistreContextMenu();
    refresh_from_storage_highlight();
  }

  function tabUpdatedHandlerHighlights(tabId, changes, tab) {
    if (
      featuresHighlights &&
      tab.url.indexOf('http') === 0 &&
      changes.status === 'complete'
    ) {
      addHighlightsContentScript(tab);
    }
  }

  async function addHighlightsContentScript(tab) {
    if (!featuresHighlights) { return; }

    // ignore chrome:// about:config etc
    if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) { return; }

    const enabled = await hasPermissions({ origins: ['*://*/*'] });

    if (enabled) {
      const result = await executeScript(tab.id, { file: 'highlight-loader.js' });
      if (!Array.isArray(result) || result.length <= 0) {
        //? Something went wrong
        helper.llog('Error loading highlights', tab, result);
        return;
      }
      const [isHighlightLoaded] = result;
      if (isHighlightLoaded === false) {
        await executeScript(tab.id, { file: 'mark.es6.min.js' });
        await executeScript(tab.id, { file: 'sha1.min.js' });
        await executeScript(tab.id, { file: 'dom-helper.js' });
        await executeScript(tab.id, { file: 'highlight.js' });
      }

      helper.doGet(
        `${highlightUrl}?url=${encodeURIComponent(tab.url)}`,
        {},
        (result) => {
          sendMessage(tab.id, { action: 'show_highlights', data: result.data });
        }
      );
    }
  }

  function onMessageHandlerHighlight(request, sender, sendResponse) {
    try {
      if (sender.tab) {
        if (request.type == 'save_highlight') {
          var data = {
            extra: {
              selection: request.highlight.selection,
            },
            url: request.highlight.url,
            title: request.highlight.title,
            text: request.highlight.text,
            color: request.highlight.color,
            tweet: request.highlight.tweet,
          };
          doPostHighlight(data, function (res) {
            if (res && res.data && res.data.redir_oauth) {
              chrome.tabs.create({ url: `${host}${res.data.redir_oauth}` });
            }
            if (res && res.errmsg === helper.errmsgs.urlTooLong) {
              res['usermsg'] = `Cannot save highlights: URL over ${helper.MAX_URL_LEN} chars.`;
            }
            sendResponse(res);
          });
          // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
          // See: Sending an asynchronous response using sendResponse
          // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
          return true;
        }
        if (request.type == 'update_highlight') {
          const data = {
            highlight_id: request.highlight.id,
            color: request.highlight.color,
          };
          doPatchHighlight(data, function (res) {
            sendResponse(res);
          });
          // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
          // See: Sending an asynchronous response using sendResponse
          // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
          return true;
        }
        if (request.type == 'delete_highlight') {
          const data = {
            highlight_id: request.highlight.id,
          };
          doDeleteHighlight(data, function (res) {
            sendResponse(res);
          });
          // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
          // See: Sending an asynchronous response using sendResponse
          // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
          return true;
        }
        if (request.type === 'setUserInfo') {
          helper.doGet(`${apiBase}/settings/`, {}, (response) => {
            if (response.data?.username && response.data?.external_id) {
              chrome.storage.local.set({
                userUsername: response.data.username,
                userExternalId: response.data.external_id,
              }, () => {
                sendResponse({ isSet: true });
              });
            } else {
              sendResponse({ isSet: false });
            }
          });
          return true;
        }
      }
    } catch (e) {
      helper.llog(e);
    }
  }

  function refresh_from_storage_highlight() {
    chrome.storage.local.get(['featuresHighlights'], (result) => {
      featuresHighlights = result.featuresHighlights;
      if (featuresHighlights) {
        enableHighlights();
      } else {
        disableHighlights();
      }
    });
  }

  chrome.runtime.onMessage.addListener(onMessageHandlerHighlight);

  chrome.storage.onChanged.addListener(() => {
    refresh_from_storage_highlight();
  });

  chrome.runtime.onInstalled.addListener(onInstalledHandlerHighlights);
  chrome.runtime.onStartup.addListener(onStartupHandlerHighlights);
})();
