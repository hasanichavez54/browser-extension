(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('sidebar', classNames);
  const host = window.histreDOMHelper.getHost();

  const utilsRefs = {
    quill: null,
    saveNote,
    saveNoteDebounced,
    tagList: null,
  };
  const utils = new window.histreUtils('sidebar', utilsRefs);
  const dataTable = utils.createExtractedDataTable();
  const votes = utils.createVoteUI(displaySpinner);
  const youtubeWatch = {
    enabled: false,
    timestamp: null,
    fullscreen: null,
  };

  const authErrorMessage = 'Unable to authenticate with histre.com';

  const sidebarWidth = 400;

  let pageUrl;
  let pageTitle;

  let quill;

  let noteLog = '';
  let noteAutosaveTimeout;

  let recentBooks = null;

  let sidebarIsOpen;
  let sidebarMoveContent;

  //? WIP move fixed elements
  // let fixedElements;

  //----- DOMPurify setup -----//

  DOMPurify.addHook('afterSanitizeAttributes', (node) => {
    // allow target="_blank"
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener noreferrer');
    }
  });

  //----- Messages -----//

  function onMessageHandler(message) {
    if (message.action === 'loading') {
      startLoadingUI();
      return;
    }

    if (message.action === 'displayCheck') {
      finishLoadingUI('', 'check');
      return;
    }

    if (message.action === 'displayError') {
      finishLoadingUI(message.message || 'An error occured', 'error');
      return;
    }

    if (message.action === 'updateVoteSidebar') {
      votes.updateVote(message.vote);
      return;
    }

    if (message.action === 'setupNote') {
      const { result, tab } = message;

      if (!result.error) {
        const { data, settings, tags, dataExtraction, vote } = result;

        if (data.item_id) {
          $wrap.dataset.histreNoteId = data.item_id;
        }

        votes.updateVote(vote);

        quill.setText('');
        if (data.note > '') {
          quill.clipboard.dangerouslyPasteHTML(
            null,
            DOMPurify.sanitize(data.note),
            'api'
          );
        }
        noteLog = data.note;

        if (!$noteAccessLevels.hasChildNodes()) {
          if (settings?.access_levels) {
            $noteAccessLevels.appendChild(utils.createAccessLevels(result.settings.access_levels, saveNote, quill));
          } else {
            $noteAccessLevels.appendChild(utils.createAccessLevels([["private", "Private"]], saveNote, quill));
          }
        }
        try {
          if (data.access) {
            $noteAccessLevels.querySelector(`input[value=${data.access}]`).checked = true;
          } else if (settings?.access_default) {
            $noteAccessLevels.querySelector(`input[value=${settings.access_default}]`).checked = true;
          } else {
            $noteAccessLevels.querySelector('input[value=private]').checked = true;
          }
        } catch {
          $noteAccessLevels.querySelector('input[value=private]').checked = true;
        }

        if (!$suggestedTagsWrap.hasChildNodes()) {
          if (data.suggested_tags && data.suggested_tags.length > 0) {
            $suggestedTagsWrap.appendChild(utils.createSuggestedTags(data.suggested_tags, quill));
          } else {
            $suggestedTagsWrap.appendChild(utils.createSuggestedTags(['toread', 'work'], quill));
          }
          $suggestedTagsWrap.style.display = 'block';
        }
        utilsRefs.tagList = tags;

        if (result.data.recent_books && result.data.recent_books.length > 0) {
          if (recentBooks) {
            recentBooks.renderRecentBooks(result.data.recent_books);
          } else {
            recentBooks = utils.createRecentBooks(result.data.recent_books, saveNote, saveNoteDebounced, quill)
            $recentBooksWrap.appendChild(recentBooks.element);
          }
        }

        $dataTableMsgWrap.style.display = 'none';
        if (dataExtraction) {
          renderExtractedData(dataExtraction);
        } else {
          renderExtractedData({ data: null });
        }

        finishLoadingUI();
      } else {
        finishLoadingUI(result.errmsg || 'An error occured', 'error');
      }

      pageUrl = encodeURIComponent(tab.url);
      pageTitle = tab.title;

      if (tab.url.startsWith('https://www.youtube.com/watch?')) {
        setupYoutubeWatch();
      } else {
        removeYoutubeWatch();
      }

      return;
    }

    if (message.action === 'updateNote') {
      const { data } = message;

      if (data.noteId) {
        $wrap.dataset.histreNoteId = data.noteId;
      }

      quill.setText('');
      if (data.note > '') {
        quill.clipboard.dangerouslyPasteHTML(
          null,
          DOMPurify.sanitize(data.note),
          'api'
        );
      }
      noteLog = data.note;

      if (data.accessLevel) {
        $noteAccessLevels.querySelector(`input[value=${data.accessLevel}]`).checked = true;
      }

      if (data.bookIds) {
        // TODO handle new book creation
        recentBooks?.updateRecentBooks(data.bookIds, data.noteId);
      }
      return;
    }

    if (message.action === 'noteSaved') {
      noteLog = message.note;
      $wrap.dataset.histreNoteId = message.noteId;
      return;
    }

    if (message.action === 'reCheckBooks') {
      recentBooks?.reCheckBooks(message.bookIds);
      return;
    }

    if (message.action === 'enableNewBookActions') {
      recentBooks?.enableNewBookActions();
      return;
    }

    if (message.action === 'enableNewBook') {
      if (message.newBookId) {
        recentBooks?.enableNewBook(message.newBookId);
      }
      return;
    }

    if (message.action === 'closeRecentBooks') {
      recentBooks?.hideRecentBooks();
      return;
    }

    if (message.action === 'showRecentBooks') {
      recentBooks?.showRecentBooks();
      return;
    }

    if (message.action === 'focusEditor') {
      quill?.focus();
      return;
    }

    if (message.action === 'renderExtractedData') {
      renderExtractedData(message.dataExtraction);
      return;
    }

    if (message.action === 'disableSidebar') {
      nukeSelf();
      return;
    }
  }

  function onStorageChangedHandler() {
    chrome.storage.local.get(['sidebarIsOpen', 'sidebarMoveContent'], (result) => {
      if (sidebarMoveContent !== result.sidebarMoveContent || sidebarIsOpen !== result.sidebarIsOpen) {
        sidebarMoveContent = result.sidebarMoveContent;
        if (result.sidebarIsOpen === true && result.sidebarMoveContent === true) {
          moveContent();
          changeMoveContentToggleIcon(true);
        } else {
          restoreContent();
          changeMoveContentToggleIcon(false);
        }
      }

      if (sidebarIsOpen !== result.sidebarIsOpen) {
        sidebarIsOpen = result.sidebarIsOpen;
        if (sidebarIsOpen) {
          $wrap.style.display = 'flex';
          document.getElementById('histre-inline-wrap').classList.add('histre-inline-d-none');
        } else {
          $wrap.style.display = 'none';
          document.getElementById('histre-inline-wrap').classList.remove('histre-inline-d-none');
        }
      }
    });
  }

  //----- Save note -----//

  function saveNote(note) {
    if (typeof note !== 'string') {
      note = DOMPurify.sanitize(quill.root.innerHTML);
    }

    const accessLevel = $noteAccessLevels.querySelector('input:checked').value;
    const bookIds = recentBooks?.getBookIdsForAddNote() || null;
    const deselectedBookIds = recentBooks?.getDeselectedBookIds() || null;
    const newBookTitle = recentBooks?.getNewBookTitle() || null;
    const noteId = $wrap.dataset.histreNoteId || null;
    const message = { action: 'saveNote', dest: 'sidebar' };

    const saveNoteRequestPayload = {
      url: pageUrl,
      title: pageTitle,
      note: note,
      book_ids: bookIds,
      access: accessLevel,
    };

    const tableValues = dataTable.getTableData();

    if (tableValues) {
      saveNoteRequestPayload['extracted_attrs'] = tableValues;
    }

    message.data = [saveNoteRequestPayload]; // must be an array

    if (deselectedBookIds?.length !== 0 && noteId) {
      message.removeNoteFromBooks = {
        book_ids: deselectedBookIds,
        url_item_item_id: noteId,
      }
    }

    if (newBookTitle) {
      message.createBook = { title: newBookTitle };
    }

    displaySpinner();

    chrome.runtime.sendMessage(message);
  }

  function saveNoteDebounced(ifChanged = false) {
    clearTimeout(noteAutosaveTimeout);

    noteAutosaveTimeout = setTimeout(() => {
      const note = DOMPurify.sanitize(quill.root.innerHTML);

      if (ifChanged) {
        if (note !== noteLog) {
          saveNote(note);
        }
      } else {
        saveNote(note);
      }
    }, 800);
  }

  //----- Status messages -----//

  function displaySpinner() {
    $checkmark.style.display = 'none';
    $alert.style.display = 'none';
    $spinner.style.display = 'inline-block';
  }

  function displayCheck() {
    $spinner.style.display = 'none';
    $checkmark.style.display = 'block';
  }

  function displayAlert() {
    $spinner.style.display = 'none';
    $alert.style.display = 'block';
  }

  function startLoadingUI() {
    displaySpinner();
    quill?.disable();
    $statusMessage.style.display = 'none';
    $statusMessage.textContent = '';
  }

  function finishLoadingUI(message = '', type = '', isFaded = false) {
    $statusMessage.textContent = message;
    $statusMessage.style.display = message ? 'block' : 'none';
    quill.enable();

    if (type === 'error') {
      $statusMessage.classList.add(prefix('status-msg--error'));
      displayAlert();
    } else if (type === 'check') {
      $statusMessage.classList.remove(prefix('status-msg--error'));
      displayCheck();
    } else {
      $statusMessage.classList.remove(prefix('status-msg--error'));
      $spinner.style.display = 'none';
      $checkmark.style.display = 'none';
      $alert.style.display = 'none';
    }

    if (message === authErrorMessage) {
      function authTryAgain() {
        chrome.runtime.sendMessage({ action: 'handleAuthError', dest: 'sidebar' });
      }

      function openHistreLogin() {
        window.open(host + '/accounts/login/');
      };

      quill?.disable();
      $statusMessage.textContent = 'Unable to authenticate with ';
      $statusMessage.appendChild($e('span', { class: prefix('auth-histre-link') }, { click: openHistreLogin }, [$t('histre.com')]));
      $statusMessage.appendChild($e('span', { class: prefix('auth-try-again') }, { click: authTryAgain }, [$t('Try again')]));
    }

    if (isFaded) {
      setTimeout(() => {
        $statusMessage.style.display = 'none';
        $statusMessage.textContent = '';
        $statusMessage.classList.remove(prefix('status-msg--error'));
        $checkmark.style.display = 'none';
        $alert.style.display = 'none';
      }, 2000);
    }
  }

  //----- Data extraction -----//

  function reExtractData() {
    $dataTableMsgWrap.style.display = 'none';
    chrome.runtime.sendMessage({ action: 'reExtractData', dest: 'sidebar' });
  }

  function renderExtractedData(extractableResult = {}) {
    const { data, errorMsg } = extractableResult;

    if (errorMsg) {
      // Currently on page with extractable data but none were found
      setMsg(errorMsg);
    } else {
      dataTable.renderExtractedData(data);
    }
  };

  //----- Youtube watch pages -----//

  function setupYoutubeWatch() {
    if (youtubeWatch.enabled === true) {
      return;
    }

    if (youtubeWatch.timestamp === null) {
      youtubeWatch.timestamp = utils.createYTTimestamp();
    }

    if (youtubeWatch.fullscreen === null) {
      youtubeWatch.fullscreen = new function () {
        this.timeout = null;
        this.inlineWrap = document.querySelector('#histre-inline-wrap');
        this.autoHideInlineUI = () => {
          clearTimeout(this.timeout);
          this.inlineWrap.classList.remove('histre-inline-hide-on-yt');
          this.timeout = setTimeout(() => {
            this.inlineWrap.classList.add('histre-inline-hide-on-yt');
          }, 3000);
        };
        this.fullscreenChangeHandler = () => {
          if (document.fullscreenElement) {
            if (!this.inlineWrap) {
              // Wait until inline UI is loaded
              const interval = setInterval(() => {
                if (this.inlineWrap) {
                  clearInterval(interval);
                  this.fullscreenChangeHandler();
                  return;
                }
                this.inlineWrap = document.querySelector('#histre-inline-wrap');
              }, 500);
              return;
            }
            this.timeout = setTimeout(() => {
              this.inlineWrap.classList.add('histre-inline-hide-on-yt');
            }, 3000);
            document.body.addEventListener('mousemove', this.autoHideInlineUI, true);
          } else {
            if (!this.inlineWrap) {
              return;
            }
            clearTimeout(this.timeout);
            this.inlineWrap.classList.remove('histre-inline-hide-on-yt');
            document.body.removeEventListener('mousemove', this.autoHideInlineUI, true);
          }
        };
      }
    }

    $editorContainer.appendChild(youtubeWatch.timestamp.element);
    $editor.addEventListener('click', youtubeWatch.timestamp.jumpToYoutubeTimpestamp);
    document.addEventListener('fullscreenchange', youtubeWatch.fullscreen.fullscreenChangeHandler);
    // If user has already entered fullscreen before event listener is set up
    if (document.fullscreenElement) {
      youtubeWatch.fullscreen.fullscreenChangeHandler();
    }
    youtubeWatch.enabled = true;
  }

  function removeYoutubeWatch() {
    if (youtubeWatch.enabled === false) {
      return;
    }

    $editorContainer.querySelector(`#${prefix('yt-timestamp')}`)?.remove();
    $editor.removeEventListener('click', youtubeWatch.timestamp.jumpToYoutubeTimpestamp);
    document.removeEventListener('fullscreenchange', youtubeWatch.fullscreen.fullscreenChangeHandler);
    youtubeWatch.enabled = false;
  }

  //----- Uncategorized Functions -----//

  function nukeSelf() {
    window.histreLoadedSidebar = false;
    clearOld();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
    chrome.storage.onChanged.removeListener(onStorageChangedHandler);
  }

  // clear elements from previous invocations
  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('sidebar.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('sidebar.css')}"]`).remove();
    }

    if (cb) { cb(); }
  };

  function closeUI() {
    const inlineWrap = document.getElementById('histre-inline-wrap');
    if (inlineWrap) {
      $wrap.style.display = 'none';
      inlineWrap.classList.remove('histre-inline-d-none');
      chrome.storage.local.set({ sidebarIsOpen: false });
      chrome.runtime.sendMessage({ action: 'sendEvent', dest: 'sidebar', data: { event: 'sidebar-min' } });
    }
  }

  function moveContent() {
    document.querySelector('html').style.width = `calc(100vw - ${sidebarWidth}px)`;

    //? WIP move fixed elements
    // if (typeof fixedElements === 'undefined') {
    //   console.time('gettingFixedElements');
    //   fixedElements = [...document.body.getElementsByTagName("*")].filter(
    //     x => getComputedStyle(x, null).getPropertyValue("position") === "fixed"
    //   ).filter((el) => el.id !== prefix('wrap') && el.id !== 'histre-inline-wrap');
    //   console.timeEnd('gettingFixedElements');
    // }
    // console.log(fixedElements);
    // fixedElements.forEach((el) => {
    //   if (getComputedStyle(el).right) {
    //     const right = getComputedStyle(el).right;
    //     el.dataset.histreFixedRight = right;
    //     el.style.right = `calc(${right} + 400px)`;
    //   }
    // });
  }

  function restoreContent() {
    document.querySelector('html').style.width = '';

    //? WIP move fixed elements
    // if (typeof fixedElements !== 'undefined') {
    //   fixedElements.forEach((el) => {
    //     if (getComputedStyle(el).right) {
    //       el.style.right = el.dataset.histreFixedRight || '0';
    //     }
    //   });
    // }
  }

  function changeMoveContentToggleIcon(isPushed) {
    if (isPushed) {
      $moveContentButton.classList.remove(prefix('control-icon--not-pushed'));
      $moveContentButton.classList.add(prefix('control-icon--pushed'));
    } else {
      $moveContentButton.classList.remove(prefix('control-icon--pushed'));
      $moveContentButton.classList.add(prefix('control-icon--not-pushed'));
    }
  }

  function toggleMoveContent() {
    chrome.storage.local.get('sidebarMoveContent', (result) => {
      chrome.storage.local.set({ sidebarMoveContent: !result.sidebarMoveContent });
    });
  }

  function openHistre() {
    window.open(host);
  };

  //----- Create UI -----//

  // Header

  const $moveContentButton = $e('button', {
    class: prefix('control-icon'),
    title: 'Overlay / Fit',
    'aria-label': 'Overlay / Fit',
  }, { click: toggleMoveContent });
  const $closeUIButton = $e('button', {
    class: prefix('control-icon control-icon--close'),
    title: 'Minimize sidebar',
    'aria-label': 'Minimize sidebar',
  }, { click: closeUI });
  const $controlButtonsWrap = $e('div', { class: prefix('control-btns-wrap') }, noEvents, [
    $moveContentButton, $closeUIButton
  ]);

  const $logoImg = $e('img', { src: chrome.runtime.getURL('assets/sidebar/logo.png'), alt: 'Histre logo' });
  const $logoTxt = $e('div', {}, noEvents, [$t('histre')]);
  const $logo = $e('div', {
    class: prefix('logo'),
    title: 'Open Histre',
    'aria-label': 'Open Histre',
  }, { click: openHistre }, [$logoImg, $logoTxt]);

  const $header = $e('div', { class: prefix('header') }, noEvents, [$logo, $controlButtonsWrap]);

  // Access levels

  const $noteAccessLevels = $e('div', { class: prefix('access-levels') });
  const $editorLabel = $e('div', { class: prefix('editor-label') }, noEvents, [$t('Add a note'), $noteAccessLevels])

  // Tags

  const $suggestedTagsWrap = $e('div', { class: prefix('suggested-tags-wrap') });

  // Editor

  const $editor = $e('div', { id: prefix('editor') });
  const $editorWrap = $e('div', { class: prefix('editor-wrap') }, noEvents, [$suggestedTagsWrap, $editor ]);
  const $editorContainer = $e('div', { class: prefix('editor-conteiner') }, noEvents, [$editorLabel, $editorWrap]);

  if (location.hostname === 'www.youtube.com') {
    $editorWrap.addEventListener('keydown', (e) => {
      if (e.altKey || e.ctrlKey || e.metaKey) {
        return;
      }
      const hijackKeys = [' ', 'k', 'm', 'j', 'l', '.', ',', '>', '<', 'f', 'c', 'i', 't', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

      if (hijackKeys.indexOf(e.key) > -1) {
        e.preventDefault();
        const selection = quill.getSelection(true);
        quill.insertText(selection.index, e.key, 'user');
        quill.setSelection(selection.index + 1);
      } else if (e.key === 'ArrowLeft') {
        e.preventDefault();
        const selection = quill.getSelection(true);
        quill.setSelection(selection.index - 1);
      } else if (e.key === 'ArrowRight') {
        e.preventDefault();
        const selection = quill.getSelection(true);
        quill.setSelection(selection.index + 1);
      }
    });
  }

  // Data extraction

  const $dataTableMsg = $e('span');
  const $dataTableTryAgainBtn = $e('span', { class: prefix('extracted-data-try-again') }, { click: reExtractData }, [$t(' Try again')])
  const $dataTableMsgWrap = $e('div', { class: prefix('extracted-data-msg') }, noEvents, [$dataTableMsg, $dataTableTryAgainBtn]);
  const $dataTableWrap = $e('div', {}, noEvents, [$dataTableMsgWrap, ...dataTable.elements]);

  // Recent books

  const $recentBooksWrap = $e('div');

  // Status messages

  const $spinner = $e('div', { class: prefix('spinner'), role: 'status', title: 'Loading...', 'aria-label': 'Loading...' });
  const $checkmark = $e('div', { class: prefix('status status--checkmark'), role: 'status', title: 'Note saved', 'aria-label': 'Note saved' });
  const $alert = $e('div', { class: prefix('status status--alert'), role: 'status', title: 'Error saving note', 'aria-label': 'Error saving note' });

  const $voteUI = $e('div', { class: prefix('vote-ui') } , noEvents, [...votes.elements])
  const $savingInfoConteiner = $e('div', { class: prefix('saving-info-container') }, noEvents, [$spinner, $checkmark, $alert, $voteUI]);

  const $statusMessage = $e('div', { class: prefix('status-msg') });

  // Footer

  const $mailLink = $e('a', { class: prefix('mail-link'), href: 'mailto:support@histre.com' }, noEvents, [$t('support@histre.com')])
  const $footer = $e('div', { class: prefix('footer') }, noEvents, [
    $t('We\'re working on adding more useful content to this sidebar. Share your suggestions and feedback with us at '),
    $mailLink,
  ]);

  // Container

  const $body = $e('div', { class: prefix('body') }, noEvents, [
    $editorContainer, $dataTableWrap, $recentBooksWrap, $savingInfoConteiner, $statusMessage
  ]);

  // Wrap

  const $wrap = $e(
    'div',
    { id: prefix('wrap'), class: prefix('wrap') },
    noEvents,
    [$header, $body, $footer]
  );

  const $css = $e(
    'link',
    {
      rel: 'stylesheet',
      type: 'text/css',
      href: chrome.runtime.getURL('sidebar.css'),
    }
  );

  //----- Setup UI -----//

  clearOld(() => {
    chrome.storage.local.get(['sidebarIsOpen', 'sidebarMoveContent'], (result) => {
      if (result.sidebarIsOpen) {
        if (result.sidebarMoveContent === true) {
          moveContent();
        }
        $wrap.style.display = 'flex';
      } else {
        restoreContent();
        $wrap.style.display = 'none';
      }

      sidebarIsOpen = result.sidebarIsOpen;
      sidebarMoveContent = result.sidebarMoveContent;

      changeMoveContentToggleIcon(result.sidebarMoveContent);

      (document.head || document.documentElement).appendChild($css);
      document.body.appendChild($wrap);

      // Cached for "addRange(): The given range isn't in document." error fix
      // ref: https://github.com/kirubakaran/histre/pull/2452
      if (typeof window.histreQuillModules !== 'object') {
        const mention = utils.getMentionConfig();

        const bindings = {
          // "Cmd/Ctrl Enter" to save and close
          custom: {
            key: 13,
            shortKey: true,
            handler() {
              saveNote();
            },
          },
        };

        window.histreQuillModules = {
          toolbar: false,
          keyboard: {
            bindings,
          },
          mention,
          magicUrl: true,
        };
      }

      quill = new Quill(`#${prefix('editor')}`, {
        modules: window.histreQuillModules,
      });

      utilsRefs.quill = quill;

      quill.on('text-change', (delta, oldDelta, source) => {
        if (source === 'api') {
          // pass
        } else if (source === 'user') {
          saveNoteDebounced(true);
        }
      });

      startLoadingUI();
    });
  });

  chrome.runtime.onMessage.addListener(onMessageHandler);
  chrome.storage.onChanged.addListener(onStorageChangedHandler);

  return true;
})();
